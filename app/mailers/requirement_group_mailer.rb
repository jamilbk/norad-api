# frozen_string_literal: true

class RequirementGroupMailer < ApplicationMailer
  def deleted(name, user)
    @name = name

    mail(
      to: admin_user_emails(user),
      subject: "#{SUBJECT_PREFIX} A Requirement Group is no longer available."
    )
  end

  private

  def admin_user_emails(user)
    user.organizations.map(&:admins).flatten(1).map(&:email).uniq
  end
end
