# frozen_string_literal: true

module V1
  class MachinesController < ApplicationController
    before_action :set_machine, only: %i[show update destroy]
    before_action :set_organization, only: %i[index create]

    def index
      render json: @org.machines
    end

    def show
      authorize_action_for @machine
      render json: @machine, serializer: MachineShowSerializer
    end

    def create
      machine = @org.machines.build(machine_params)
      if machine.save
        render json: machine
      else
        render_errors_for(machine)
      end
    end

    def update
      authorize_action_for @machine
      if @machine.update(machine_params)
        render json: @machine
      else
        render_errors_for(@machine)
      end
    end

    def destroy
      authorize_action_for @machine
      @machine.destroy

      head :no_content
    end

    private

    def set_machine
      @machine = Machine.find(params[:id])
    end

    def machine_params
      params.require(:machine).permit(:ip, :fqdn, :description, :name, :use_fqdn_as_target)
    end

    def set_organization
      @org = Organization.find params[:organization_id]
      authorize_action_for Machine, in: @org
    end
  end
end
