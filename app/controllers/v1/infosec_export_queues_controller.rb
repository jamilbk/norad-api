# frozen_string_literal: true

module V1
  class InfosecExportQueuesController < ResultExportQueuesController
    private

    def result_export_queue_params
      params.require(:infosec_export_queue).require(*allowed_nested_attributes.keys)
      params.require(:infosec_export_queue)
            .permit(:auto_sync, allowed_nested_attributes)
    end

    def allowed_nested_attributes
      { infosec_export_queue_configuration_attributes: %i[id ctsm_id] }
    end

    def child_create
      infosec_export_queue = InfosecExportQueue.find_by!(organization: organization)
      render json: infosec_export_queue, location: v1_infosec_export_queue_url(infosec_export_queue),
             status: :not_modified
    rescue ActiveRecord::RecordNotFound
      infosec_export_queue = InfosecExportQueue.new(result_export_queue_creation_params)
      if infosec_export_queue.save
        render json: infosec_export_queue, status: :created
      else
        render_errors_for(infosec_export_queue)
      end
    end
  end
end
