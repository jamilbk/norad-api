# frozen_string_literal: true

module V1
  class ApiTokensController < V1::ApplicationController
    before_action :set_token, only: :update

    def update
      @token.recreate!
      render json: @token
    end

    private

    def set_token
      user = User.find(params[:user_id])
      authorize_action_for user
      @token = user.api_token
    end
  end
end
