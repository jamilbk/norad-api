# frozen_string_literal: true

module V1
  class JiraExportQueuesController < ResultExportQueuesController
    private

    def result_export_queue_params
      params.require(:jira_export_queue).require(*allowed_nested_attributes.keys)
      params.require(:jira_export_queue)
            .permit(:auto_sync, allowed_nested_attributes)
    end

    def allowed_nested_attributes
      { custom_jira_configuration_attributes: %i[id title site_url project_key username password] }
    end

    def child_create
      jira_export_queue = JiraExportQueue.new(result_export_queue_creation_params)

      if jira_export_queue.save
        render json: jira_export_queue, status: :created
      else
        render_errors_for(jira_export_queue)
      end
    end
  end
end
