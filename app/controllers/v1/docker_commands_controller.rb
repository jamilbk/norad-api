# frozen_string_literal: true

module V1
  class DockerCommandsController < ApplicationController
    include BelongsToPolyParent
    before_action :set_record, only: %i[show destroy]
    before_action :set_parent_object, only: %i[create index latest]
    before_action :set_render_opts, only: %i[show index]

    undef_method :update

    def latest
      @record = @parent.latest_command.tap { |dc| raise ActiveRecord::RecordNotFound unless dc }
      render_latest_docker_command
    end

    private

    def set_render_opts
      @index_render_opts = @show_render_opts = {
        include: ['security_containers.security_test_repository', 'assessments']
      }
    end

    def render_latest_docker_command
      render(
        {
          json: @record,
          include: ['assessments.results', 'security_containers.security_test_repository']
        }.merge(filter_options)
      )
    end

    def filter_options
      @parent.is_a?(Machine) ? { machine_filter_id: @parent.id } : {}
    end

    def creation_params
      params.require(:docker_command).permit(scan_containers_attributes: [:security_container_id])
    end
  end
end
