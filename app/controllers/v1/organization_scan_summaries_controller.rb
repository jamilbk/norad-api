# frozen_string_literal: true

module V1
  class OrganizationScanSummariesController < ApplicationController
    before_action :load_organization

    # Show scan summaries for an organization
    #
    # GET /organizations/{organization.slug}/scan_summary
    def show
      summary = OrganizationScanSummary.new(organization: @organization)
      authorize_action_for summary, in: @organization
      render json: summary
    end

    private

    def load_organization
      @organization = Organization.find(params[:organization_id])
    end
  end
end
