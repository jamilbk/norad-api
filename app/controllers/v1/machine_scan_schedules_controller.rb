# frozen_string_literal: true

module V1
  class MachineScanSchedulesController < V1::ApplicationController
    before_action :set_schedule, only: %i[show update destroy]
    before_action :set_machine, only: %i[index create]

    def index
      render json: @machine.scan_schedules
    end

    def create
      schedule = @machine.scan_schedules.build(schedule_params)
      if schedule.save
        render json: schedule
      else
        render_errors_for(schedule)
      end
    end

    def show
      render json: @schedule
    end

    def update
      if @schedule.update(schedule_params)
        render json: @schedule
      else
        render_errors_for(@schedule)
      end
    end

    def destroy
      @schedule.destroy
      head :no_content
    end

    private

    def set_schedule
      @schedule = MachineScanSchedule.find(params[:id])
      authorize_action_for @schedule
    end

    def set_machine
      @machine = Machine.find(params[:machine_id])
      authorize_action_for MachineScanSchedule, in: @machine.organization
    end

    def schedule_params
      params.require(:machine_scan_schedule).permit(:period, :at)
    end
  end
end
