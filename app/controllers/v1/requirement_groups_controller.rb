# frozen_string_literal: true

module V1
  class RequirementGroupsController < V1::ApplicationController
    authorize_actions_for RequirementGroup, only: %i[index create]
    before_action :set_and_authorize_group, only: %i[show update destroy]

    def index
      render json: requirement_groups
    end

    def show
      render json: @group
    end

    def create
      @req_group = RequirementGroup.new(group_params)
      ActiveRecord::Base.transaction do
        @req_group.save!
        current_user.add_role :requirement_group_admin, @req_group
      end
      render json: @req_group, serializer: RequirementGroupSerializer
    rescue ActiveRecord::RecordInvalid
      render_errors_for(@req_group)
    end

    def update
      if @group.update(group_params)
        render json: @group
      else
        render_errors_for(@group)
      end
    end

    def destroy
      @group.destroy!
      RequirementGroupMailer.deleted(@group.name, current_user).deliver_later
      head :no_content
    end

    private

    def group_params
      params.require(:requirement_group).permit(:name, :description)
    end

    def set_and_authorize_group
      @group = RequirementGroup.find(params[:id])
      authorize_action_for @group
    end

    def requirement_groups
      params[:with_admin].blank? ? RequirementGroup.all : RequirementGroup.with_role(:requirement_group_admin)
    end
  end
end
