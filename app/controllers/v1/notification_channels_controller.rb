# frozen_string_literal: true

module V1
  class NotificationChannelsController < ApplicationController
    before_action :set_organization, only: :index
    before_action :set_notification_channel, except: :index

    def index
      render json: @org.notification_channels
    end

    def show
      render json: @notification_channel
    end

    def update
      if @notification_channel.update(notification_channel_params)
        render json: @notification_channel
      else
        render_errors_for(@notification_channel)
      end
    end

    private

    def notification_channel_params
      params.require(:notification_channel).permit(:enabled)
    end

    def set_notification_channel
      @notification_channel = NotificationChannel.find(params[:id])
      authorize_action_for @notification_channel
    end

    def set_organization
      @org = Organization.find(params[:organization_id])
      authorize_action_for NotificationChannel, in: @org
    end
  end
end
