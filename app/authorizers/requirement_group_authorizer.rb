# frozen_string_literal: true

class RequirementGroupAuthorizer < ApplicationAuthorizer
  def self.creatable_by?(_user)
    true
  end

  def self.readable_by?(_user)
    true
  end

  def readable_by?(_user)
    true
  end

  def updatable_by?(user)
    admin?(user)
  end

  def deletable_by?(user)
    admin?(user)
  end

  def admin?(user)
    user.has_role? :requirement_group_admin, resource
  end
end
