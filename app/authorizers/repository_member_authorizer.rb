# frozen_string_literal: true

class RepositoryMemberAuthorizer < ApplicationAuthorizer
  class << self
    def creatable_by?(user, options)
      repository = options.fetch(:in)
      admin?(user, repository)
    end

    def readable_by?(user, options)
      repository = options.fetch(:in)
      admin?(user, repository)
    end

    private

    def admin?(user, repository)
      user.has_role? :security_test_repository_admin, repository
    end
  end

  def deletable_by?(user)
    admin?(user) && resource.user != user
  end

  private

  def admin?(user)
    user.has_role? :security_test_repository_admin, resource.security_test_repository
  end
end
