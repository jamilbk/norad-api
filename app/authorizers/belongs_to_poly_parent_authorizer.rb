# frozen_string_literal: true

# This authorizer wraps up common access controls for RESTful Org / Machine STI-enabled children
class BelongsToPolyParentAuthorizer < ApplicationAuthorizer
  class << self
    # index
    def readable_by?(user, options)
      organization = options.fetch(:in).rbac_parent
      admin?(user, organization) || reader?(user, organization)
    end

    # create
    def creatable_by?(user, options)
      organization = options.fetch(:in).rbac_parent
      admin?(user, organization)
    end
  end

  # show
  def readable_by?(user)
    admin?(user, org) || reader?(user, org)
  end

  # update
  def updatable_by?(user)
    admin?(user, org)
  end

  # destroy
  def deletable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.send(poly_assoc_name).rbac_parent
  end
end
