# frozen_string_literal: true

class OrganizationConfigurationAuthorizer < ApplicationAuthorizer
  def updatable_by?(user)
    admin?(user, org)
  end

  def readable_by?(user)
    admin?(user, org)
  end

  private

  def org
    resource.organization
  end
end
