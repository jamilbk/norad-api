# frozen_string_literal: true

class SummaryQuery
  # { passing: 0, failing: 0, erroring: 0, warning: 0, informing: 0 }
  DEFAULT_STATUSES = Result::HUMAN_STATUSES.values.map { |s| [s, 0] }.to_h.freeze

  attr_reader :relation

  def initialize(options = {})
    @relation = options.fetch(:relation, nil)
  end

  private

  # XXX: Postgres doesn't return a count of 0 for missing results, so we need to initialize the missing results
  # ourselves. The better way to do this is Assessment.left_outer_joins(:results) but there are two issues with this
  # approach:
  # 1. Rails won't auto-load the Result status string from the enum index, e.g. returning 2 instead of "erroring" when
  #    initializing the record objects from the results of this query
  # 2. There's no easy way to have Postgres initialize a set of result counts for us. [dc_id, nil] => 0 will be returned
  #    for missing results, when in reality we want something like,
  #    [dc_id, "fail"] => 0, [dc_id, "pass"] => 0, [dc_id, "error"] => 0, [dc_id, "info"] => 0, [dc_id, "warn"] => 0
  def reduced_by_result_counts(grouping = primary_grouping_clause)
    counts = Result
             .not_ignored
             .joins(:assessment)
             .where(assessments: assessments_scope_hash)
             .group(grouping, :status)
             .count(:status) # Gives us { [dc_id, status] => count, ... , }

    initialized_result_counts.merge(counts)
  end

  # Initializes a Hash containing result counts of 0 for all docker command ids relevant to assessments_scope_hash
  # E.g. { [dc_id1, status] => 0, [dc_id2, status] => 0 }
  #
  # This is needed when a scan summary has been requested but no results have been created.
  def initialized_result_counts
    dc_or_machine_ids =
      Assessment
      .where(assessments: assessments_scope_hash)
      .pluck(primary_grouping_clause)

    dc_or_machine_ids.product(Result::HUMAN_STATUSES.keys).product([0]).to_h
  end
end
