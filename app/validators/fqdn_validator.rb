# frozen_string_literal: true

module FqdnValidator
  extend ActiveSupport::Concern

  included do
    validates :fqdn, presence: true, if: proc { |a| a.ip.blank? }
    validates :fqdn, uniqueness: { scope: :organization_id }, allow_blank: true
    validates :fqdn, exclusion: { in: ['localhost'], message: 'cannot be localhost' }
    validates(
      :fqdn,
      format: {
        with: /\A((?=_?[a-z0-9-]{1,63}\.)(xn--)?(_?[a-z0-9]+)(-[a-z0-9]+)*\.)+[a-z][a-z0-9]{0,62}\z/,
        message: 'must be a valid FQDN'
      },
      allow_blank: true
    )
  end
end
