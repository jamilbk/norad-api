# frozen_string_literal: true

class ResultExporter
  include Authority::Abilities

  attr_reader :export_queue, :results

  def initialize(export_queue, result_ids)
    @export_queue = export_queue
    @results = Result.where(id: result_ids).includes(assessment: { machine: :organization })
  end

  def publish
    # Publish to the default exchange
    publish_retval = Publisher.publish('', results_data, queue_name)
    raise "Error publishing results to #{queue_name}" unless publish_retval
    publish_retval
  end

  def results_data
    {
      results: results.map { |result| ResultExporterSerializers::Result.new(result) },
      export_target: ResultExporterSerializers::ExportTarget.new(export_queue)
    }
  end

  private

  def queue_name
    export_queue.amqp_queue_name
  end
end

module ResultExporterSerializers
  class Result < ActiveModel::Serializer
    attributes :data, :meta

    def data
      ::ResultSerializer.new(object)
    end

    def meta
      assessment = object.assessment
      {
        assessment: assessment.identifier,
        organization: Organization.new(assessment.machine.organization),
        machine: Machine.new(assessment.machine),
        service: serialized_service(assessment.service),
        scan_id: assessment.docker_command_id
      }
    end

    private

    def serialized_service(service)
      return {} if service.nil?
      Service.new(service)
    end
  end

  class Machine < ActiveModel::Serializer
    attributes :id, :ip, :fqdn, :description, :name, :use_fqdn_as_target
  end

  class Organization < ActiveModel::Serializer
    attributes :id, :uid
  end

  class Service < ActiveModel::Serializer
    attributes :id, :name, :port, :port_type, :encryption_type, :type
  end

  class ExportTarget < ActiveModel::Serializer
    attributes :target_connection_settings
  end
end
