# frozen_string_literal: true

class RelayImageInfoFetcher
  VERSION_EXPIRES_IN = 5.minutes

  class << self
    def fetch_and_cache_latest_version
      # This doesn't really make sense in development
      return nil if Rails.env.development?

      Rails.cache.fetch('relay_version', expires_in: VERSION_EXPIRES_IN) do
        fetch_latest_version
      end
    end

    def cached_latest_version
      Rails.cache.fetch('relay_version')
    end

    private

    # Retrieves the list of tags for the relay, then checks if any of the
    # explicitly-tagged relay versions (e.g. 0.0.3) match the image id for the
    # 'latest' tag. If the 'latest' tag does not match any known versions,
    # we can't determine what the latest version is.
    def fetch_latest_version
      numerical_tag = latest_numerical_tag

      latest_id = image_id_from_tag 'latest'
      numerical_id = image_id_from_tag numerical_tag

      only_numerical = numerical_id && !latest_id
      ids_match = latest_id == numerical_id

      return numerical_tag if only_numerical || ids_match
    end

    def image_id_from_tag(tag)
      return nil unless tag
      image_manifest = manifest(tag)
      image_manifest.fetch('config').fetch('digest') if image_manifest
    end

    def latest_numerical_tag
      # The Gem class ships with Ruby
      numerical_tags.sort_by { |tag| Gem::Version.new(tag) }.last
    end

    # Filters tags which are in the format '0.0.1'
    def numerical_tags
      tags.keep_if { |tag| DockerRelay::VERSION_FORMAT.match(tag) }
    end

    def tags
      DockerRegistry.json_request('/relay/tags/list')['tags']
    end

    def manifest(tag)
      DockerRegistry.json_request "/relay/manifests/#{tag}"
    end
  end
end
