# frozen_string_literal: true

class ContainerEnvironmentBuilder
  class << self
    def build_options_hash(name, generic_container, args, targets, secret)
      {
        'name' => name,
        'Image' => generic_container.full_path,
        'Env' => env_variables(targets, secret),
        'Cmd' => args
      }
    end

    def build_connectivity_check_environment(check)
      build_connectivity_base_environment(check) + build_secret_environment(check.shared_secret)
    end

    def build_discovery_environment(discovery)
      services_url = Rails.application.routes.url_helpers.v1_machine_services_path(discovery.machine_id)
      discovery_url = Rails.application.routes.url_helpers.v1_service_discovery_path(discovery)
      [
        "NORAD_ROOT=#{api_url}",
        "NORAD_SERVICES_URL=#{services_url}",
        "NORAD_DISCOVERY_URL=#{discovery_url}",
        "NORAD_SECRET=#{discovery.shared_secret}"
      ]
    end

    private

    def build_connectivity_base_environment(check)
      [
        "NORAD_ROOT=#{api_url}",
        "NORAD_CONNECTIVITY_CHECK_PATH=#{check_path(check)}"
      ]
    end

    def build_secret_environment(secret)
      ["NORAD_SECRET=#{secret}"]
    end

    def env_variables(targets, secret)
      [
        "NORAD_ROOT=#{api_url}",
        "ASSESSMENT_PATHS=#{targets.to_json}",
        "NORAD_SECRET=#{secret}"
      ]
    end

    def api_url
      ENV.fetch('API_URL').gsub(%r{/v1}, '')
    end

    def organization_errors_path
      Rails.application.routes.url_helpers.v1_organization_errors_path
    end

    def check_path(check)
      path_method = "v1_#{check.class.name.underscore}_path"
      Rails.application.routes.url_helpers.send(path_method, check)
    end
  end
end
