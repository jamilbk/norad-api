# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

class Service < ApplicationRecord
  # RBAC
  include Authority::Abilities

  # Attribute Information
  enum port_type: { tcp: 0, udp: 1 }
  enum encryption_type: { cleartext: 0, ssl: 1, ssh: 2 }
  # Note that in the future we may wish to look for a way to generate this list dynamically if the
  # number of subclasses grows large. For now, it's feasible to explicitly list them.
  VALID_TYPES = %w[GenericService SshService WebApplicationService].freeze

  # Associations
  belongs_to :machine, inverse_of: :services
  belongs_to :application_type, inverse_of: :services
  has_one :service_identity
  has_many :assessments, inverse_of: :service

  # Validations
  validates :machine, presence: true
  validates :name, presence: true
  validates :port, presence: true
  validates :port, inclusion: { in: 1..65_535, message: 'must be a number between 1 and 65,535' }
  validates :type, inclusion: { in: VALID_TYPES, message: "must be one of [#{VALID_TYPES.join(', ')}]" }
  validates :port, uniqueness: { scope: :machine_id }

  delegate :organization, to: :machine

  # Callbacks
  before_save :set_app_type

  after_initialize do |service|
    service.infer_type_by_port
    service.service_identity&.service = service
  end

  accepts_nested_attributes_for :service_identity, reject_if: :all_blank

  def application_type_attributes=(attributes)
    return set_app_type unless attributes && attributes[:name] && attributes[:port] && attributes[:transport_protocol]

    self.application_type = ApplicationType.find_by(
      name: attributes[:name],
      port: attributes[:port],
      transport_protocol: attributes[:transport_protocol]
    )
  end

  # A machine's services are passed to the machine connectivity container
  include OrganizationErrorWatcher
  watch_for_organization_errors UnableToPingMachineError, subject_method: :machine, attributes: %i[port port_type]
  watch_for_organization_errors KeyPairAssignmentWithoutSshServiceError,
                                SshServiceWithoutKeyPairAssignmentError,
                                on: %i[create destroy],
                                if: -> { type == 'SshService' },
                                subject_method: :machine
  watch_for_organization_errors UnableToSshToMachineError,
                                on: %i[create update],
                                if: -> { type == 'SshService' },
                                attributes: :port,
                                subject_method: :machine
  destroy_associated_organization_errors UnableToSshToMachineError,
                                         subject_method: :machine,
                                         if: -> { type == 'SshService' }

  # I'm disabling these cops because I think this is the most readable way to write this
  # functioanlity. I am open to any suggestions to improve it
  # rubocop:disable Metrics/MethodLength
  # rubocop:disable Metrics/AbcSize
  def self.testable_by(container)
    if container.authenticated?
      where(type: 'SshService').of_application_type(container.application_type_id)
    elsif container.web_application?
      where(type: 'WebApplicationService').of_application_type(container.application_type_id)
    elsif container.brute_force?
      where(allow_brute_force: true).of_application_type(container.application_type_id)
    elsif container.ssl_crypto?
      ssl.of_application_type(container.application_type_id)
    elsif container.ssh_crypto?
      ssh.of_application_type(container.application_type_id)
    else # we should never reach this, but just in case...
      none
    end
  end
  # rubocop:enable Metrics/MethodLength
  # rubocop:enable Metrics/AbcSize

  def self.of_application_type(type_id)
    return all unless type_id
    where('application_type_id = ? OR application_type_id IS NULL', type_id)
  end

  def service_configuration
    {
      port: port,
      service_username: service_identity&.username,
      service_password: service_identity&.password
    }
  end

  def infer_type_by_port
    return unless discovered?
    return self.type = 'WebApplicationService' if WebApplicationService::COMMON_WEB_PORTS.include? port
    return self.type = 'SshService' if SshService::COMMON_SSH_PORTS.include? port
    self.type = 'GenericService'
  end

  def web_app?
    type == 'WebApplicationService'
  end

  private

  # Callback definitions
  def set_app_type
    self.application_type ||= ApplicationType.find_by(port: port, transport_protocol: port_type)
  end
end
