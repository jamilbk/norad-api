# frozen_string_literal: true

class Token < ApplicationRecord
  self.abstract_class = true

  # Callback declarations
  around_create :generate_token_value

  def recreate!
    generate_token_value { save! }
  end

  private

  # Callback Definitions
  def generate_token_value
    self.value = SecureRandom.hex 32 # generates a secure random hex string of length 64
    yield
  end
end
