# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

class UnableToSshToMachineError < MachineError
  CHECK_KLASS = 'SshConnectivityCheck'

  def message
    if machine.organization.configuration.use_relay_ssh_key
      "Unable to ssh to machine #{machine_name} using its Organization's Relay SSH key"
    elsif machine.ssh_key_pair.present?
      "Unable to ssh to machine #{machine_name} using SSH Key Pair #{machine.ssh_key_pair.name}"
    else
      "Unable to ssh to machine #{machine_name}"
    end
  end

  private

  def machine_name
    machine&.name
  end
end
