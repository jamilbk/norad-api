# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_connectivity_checks
#
#  id                           :integer          not null, primary key
#  machine_id                   :integer          not null
#  security_container_secret_id :integer
#  finished_at                  :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  status                       :integer
#  type                         :string           not null
#  status_reason                :text
#
# Indexes
#
#  index_machine_connectivity_checks_on_finished_at          (finished_at)
#  index_machine_connectivity_checks_on_machine_id_and_type  (machine_id,type)
#  index_machine_connectivity_checks_on_s_c_s_id             (security_container_secret_id) UNIQUE
#  index_machine_connectivity_checks_on_status               (status)
#  index_machine_connectivity_checks_on_updated_at           (updated_at)
#
# Foreign Keys
#
#  fk_rails_74009af901  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7a47e669da  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#

class PingConnectivityCheck < MachineConnectivityCheck
  CONTAINER_NAME = 'ping-connectivity-check'
  CONTAINER_VERSION = 'latest'
  ORG_ERROR_KLASS = 'UnableToPingMachineError'

  def container_args
    hash = Hash.new { |h, k| h[k] = [] }
    machine.services.each { |service| hash[service.port_type] << service.port }
    [
      machine.target_address,
      hash.to_json
    ]
  end
end
