# frozen_string_literal: true

# == Schema Information
#
# Table name: security_containers
#
#  id                          :integer          not null, primary key
#  name                        :string           not null
#  category                    :integer          default("whitebox"), not null
#  prog_args                   :string           not null
#  default_config              :jsonb
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  multi_host                  :boolean          default(FALSE), not null
#  test_types                  :string           default([]), not null, is an Array
#  configurable                :boolean          default(FALSE), not null
#  application_type_id         :integer
#  help_url                    :string
#  security_test_repository_id :integer          not null
#
# Indexes
#
#  index_security_containers_on_application_type_id          (application_type_id)
#  index_security_containers_on_s_t_r_id_and_name            (security_test_repository_id,name) UNIQUE
#  index_security_containers_on_security_test_repository_id  (security_test_repository_id)
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (application_type_id => application_types.id)
#  fk_rails_c5578062aa  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

class SecurityContainer < ApplicationRecord
  # RBAC
  include Authority::Abilities
  resourcify

  attr_writer :service

  # Attribute Information
  enum category: %i[whitebox blackbox]

  SPACE_PLACEHOLDER = '~!_NORAD_SPACE_PLACEHOLDER_!~'
  TEST_TYPES = %w[authenticated web_application brute_force ssl_crypto ssh_crypto whole_host].sort

  # Validations
  validates :name,
            presence: true,
            format: {
              with: %r{\A[A-Za-z][\w@:./-]+\z},
              message: 'can only contain @, alphanumeric, colons, periods, underscores and dashes'
            }
  validates :prog_args, presence: true
  validates :test_types, presence: true

  # Custom Validations
  include SecurityContainerValidator

  # Associations
  has_many :assessments, inverse_of: :security_container
  has_many :security_container_configs, inverse_of: :security_container
  has_many :provisions, inverse_of: :security_container
  has_many :requirements, through: :provisions, inverse_of: :security_containers
  has_many :scan_containers, inverse_of: :security_container
  has_many :docker_commands, through: :scan_containers, inverse_of: :security_containers
  belongs_to :application_type, inverse_of: :security_containers
  belongs_to :security_test_repository, inverse_of: :security_containers, optional: false

  delegate :official?, to: :security_test_repository

  scope(:official_containers,
        -> { joins(:security_test_repository).where(security_test_repositories: { official: true }) })

  # Callbacks
  before_validation do |sc|
    test_types && sc.test_types.uniq!
    self.default_config ||= {}
  end
  before_validation :associate_application_type

  # Miscellaneous Instance Methods
  def args_hash
    default_config.each_with_object({}) do |(k, v), h|
      h[k.to_sym] = v.to_s.gsub(/ /, SPACE_PLACEHOLDER)
    end
  end

  def config_for_machine(id)
    security_container_configs.for_machine(id)
  end

  def config_for_organization(id)
    security_container_configs.for_organization(id)
  end

  def full_path
    if security_test_repository.host.present?
      [security_test_repository.host, name].join('/')
    else
      name
    end
  end

  # Define methods like #whole_host? and #authenticated?
  TEST_TYPES.each do |ttype|
    define_method :"#{ttype}?" do
      test_types.include? ttype
    end
  end

  private

  # Attempts to associate user-supplied service details to an existing application type record
  def associate_application_type
    return nil unless @service && @service[:port] && @service[:transport_protocol]
    self.application_type = ApplicationType.find_by(@service.compact)
  end
end
