# frozen_string_literal: true

# Decorator for the Organization model
class OrganizationWithLatestResults < SimpleDelegator
  def initialize(organization)
    super
    @organization_results_query = OrganizationResultsQuery.new(Organization.where(id: organization.id))
    @machine_results_query = MachineResultsQuery.new(machines)
    @machine_commands_query = MachineCommandsQuery.new(machines)
  end

  def latest_results
    more_recent_results_for_machines.or results_from_most_recent_org_scan
  end

  private

  attr_reader :organization_results_query, :machine_results_query, :machine_commands_query

  def results_from_most_recent_org_scan
    organization_results_query.for_command_excluding_machines(latest_command, machine_ids_with_recent_scans)
  end

  def latest_command
    docker_commands.most_recent
  end

  def machine_ids_with_recent_scans
    more_recent_commands_for_machines.pluck(:machine_id)
  end

  def more_recent_commands_for_machines
    machine_commands_query.unique_commands_newer_than(latest_command&.created_at)
  end

  def more_recent_results_for_machines
    machine_results_query.for_commands(more_recent_commands_for_machines.pluck(:id))
  end
end
