# frozen_string_literal: true

# == Schema Information
#
# Table name: organizations
#
#  id            :integer          not null, primary key
#  uid           :string           not null
#  slug          :string           not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#  exchange_name :uuid             not null
#
# Indexes
#
#  index_organizations_on_exchange_name  (exchange_name) UNIQUE
#  index_organizations_on_slug           (slug) UNIQUE
#  index_organizations_on_uid            (uid) UNIQUE
#

# rubocop:disable Metrics/ClassLength
class Organization < ApplicationRecord
  include WithPaddedDuration
  include NotificationInitializable

  DEFAULT_SUFFIX = 'default'
  UID_REGEX = /\A[A-Za-z][ \w-]+\z/

  attr_accessor :default_org

  # RBAC
  include Authority::Abilities
  resourcify

  # Attribute Information
  def to_param
    slug
  end

  # Validations
  validates :uid,
            presence: true,
            format: { with: UID_REGEX, message: 'must be alphanumeric (dashes and spaces are ok)' }
  validate :reserved_word_validation, if: ->(org) { org.default_org != true }

  # Associations
  has_many :memberships
  has_many :users, through: :memberships
  has_many :machines, inverse_of: :organization
  has_many :docker_commands, inverse_of: :organization
  has_many :security_container_configs, inverse_of: :organization
  has_one :configuration, inverse_of: :organization, class_name: 'OrganizationConfiguration'
  has_one :iaas_configuration, inverse_of: :organization
  has_many :iaas_discoveries, through: :iaas_configuration
  has_many :docker_relays, inverse_of: :organization
  has_many :enforcements
  has_many :requirement_groups, through: :enforcements
  has_many :requirements, through: :requirement_groups
  has_many :scan_schedules, inverse_of: :organization, class_name: 'OrganizationScanSchedule'
  has_many :ssh_key_pairs, inverse_of: :organization
  has_many :notification_channels, inverse_of: :organization
  has_many :organization_errors, inverse_of: :organization
  has_many :result_ignore_rules, as: :ignore_scope, inverse_of: :ignore_scope, dependent: :destroy
  has_many :repository_whitelist_entries, dependent: :destroy, inverse_of: :organization
  has_many :whitelisted_repositories, through: :repository_whitelist_entries, source: :security_test_repository
  has_many :whitelisted_containers, through: :whitelisted_repositories, source: :security_containers
  has_one :unreachable_machine_error
  has_many :result_export_queues
  has_one(:organization_token, -> { active })

  # Callback declarations
  before_validation :populate_exchange_name, if: 'exchange_name.blank?'
  before_create :build_organization_token
  before_create :build_configuration
  before_save :generate_slug

  class << self
    def find(input)
      if input.to_i.nonzero?
        super
      else
        find_by(slug: input).tap { |org| raise ActiveRecord::RecordNotFound unless org }
      end
    end

    def by_token(token)
      with_padded_duration(1) do
        joins(:organization_token).find_by!(organization_tokens: { value: token })
      end
    end
  end

  def populate_exchange_name
    self.exchange_name = SecureRandom.uuid
  end

  def unreachable_machine?
    organization_errors.where(type: 'UnreachableMachineError').exists?
  end

  def token
    organization_token.try(:value)
  end

  # FIXME: Currently we can only route to one verified queue at a time. Update this method when
  # round-robin queues are implemented for Docker Relays
  def docker_relay_queue
    primary_relay&.queue_name
  end

  def primary_relay
    docker_relays.verified.last
  end

  # FIXME: Currently we can only route to one verified queue at a time. Update this method when
  # round-robin queues are implemented for Docker Relays
  def relay_encryption_key
    docker_relays.verified.last&.file_encryption_key
  end

  # Return the set of containers enabled via requirements
  def required_containers
    SecurityContainer.joins(requirements: { requirement_group: :organizations }).where(organizations: { id: id })
  end

  # List of users with the organization_admin role on the organization
  def admins
    User.with_role :organization_admin, self
  end

  def notification_enabled?(event)
    notification_channels.find_by(event: event).enabled
  end

  def rfc1918_machines
    machines.rfc1918
  end

  # Summary of assessment stats
  def machine_assessment_summary
    # Create hash with all keys represented and values of zero
    summary = Machine::RANKED_ASSESSMENT_STATUSES.each_with_object({}) { |s, h| h[s] = 0 }
    # Now tally the statuses
    machines.each { |m| summary[m.assessment_status] += 1 }
    summary
  end

  def latest_results
    OrganizationWithLatestResults.new(self).latest_results
  end

  def latest_command
    docker_commands.order(:created_at).last
  end

  # Organizations inherit their RBAC rules from themselves
  def rbac_parent
    self
  end

  def auto_export_results(completed_scan)
    result_ids = ResultsQuery.new.for_commands(completed_scan).distinct.pluck(:id)
    result_export_queues.where(auto_sync: true).find_each { |q| q.export(result_ids) }
  end

  def contains_enabled_tests?
    contains_enabled_security_containers? || contains_enabled_requirements?
  end

  private

  def contains_enabled_requirements?
    requirements.exists?
  end

  def contains_enabled_security_containers?
    SecurityContainerConfig.where(organization_id: id).or(SecurityContainerConfig.where(machine_id: machines)).exists?
  end

  # Callback Definitions

  def generate_slug
    self.slug = uid.parameterize
    # Try 3 times to generate a unique slug. Otherwise, PG will trigger ActiveRecord::RecordNotUnique for us
    3.times { self.class.where(slug: slug).exists? ? self.slug += format('_%#.4d', rand(1..1000)) : return }
  end

  # Custom Validations
  def reserved_word_validation
    return unless uid&.match?(/#{DEFAULT_SUFFIX}\z/i)
    errors.add :uid, 'cannot end with default'
  end
end
