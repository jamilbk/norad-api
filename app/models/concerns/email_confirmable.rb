# frozen_string_literal: true

module EmailConfirmable
  extend ActiveSupport::Concern

  included do
    has_secure_token :email_confirmation_token

    def confirm!
      update!(email_confirmed_at: DateTime.current)
      user.send_welcome_email
    end

    def confirmed?
      email_confirmed_at.present?
    end
  end
end
