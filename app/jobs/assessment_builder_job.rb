# frozen_string_literal: true

require './lib/norad_exception_notifier'
require './lib/norad_relay_helpers'
require './lib/norad_scan_builder'

class AssessmentBuilderJob < ApplicationJob
  include NoradExceptionNotifier

  queue_as :command_processing

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)
    Resque.logger.error "Exception while starting job: #{exception.inspect}" if defined?(Resque)
    # XXX: The first argument passed to the perform method is the docker command.
    command = @arguments.first
    ActiveRecord::Base.transaction do
      command.error_details = 'Something went wrong while queuing the containers'
      command.cancel!
    end
  end

  before_perform do
    command = @arguments.first
    command.update_column(:started_at, DateTime.current)
  end

  after_perform do
    @dc.complete!
  end

  def perform(docker_command)
    @dc = docker_command
    jobs(@dc.security_containers).flatten.each do |job|
      job.scans.reject(&:empty?).each do |scan|
        ScheduleContainerJob.perform_later(job.to_h.merge(scan.to_h))
      end
    end
  end

  private

  def jobs(containers)
    @dc.commandable.class.name == 'Organization' ? build_for_org(containers) : build_for_machine(containers)
  end

  def build_for_org(containers)
    org = @dc.commandable
    containers.map do |c|
      if enabled_by_org?(c, org)
        next queue_multi_host(c, org) if c.multi_host
        org.machines.map { |m| NoradScanBuilder::MachineScanJob.new(m, c, @dc) }
      else
        org.machines.with_configuration_for(c).map { |m| NoradScanBuilder::MachineScanJob.new(m, c, @dc) }
      end
    end
  end

  def queue_multi_host(container, org)
    special_machines = multi_host_special_machines(container, org)
    machine_jobs = special_machines.map do |m|
      NoradScanBuilder::MachineScanJob.new(m, container, @dc)
    end
    return machine_jobs if org.machines.where.not(id: special_machines).empty?
    machine_jobs << NoradScanBuilder::OrganizationScanJob.new(org, container, special_machines, @dc)
  end

  def build_for_machine(containers)
    containers.map do |c|
      NoradScanBuilder::MachineScanJob.new(@dc.commandable, c, @dc)
    end
  end

  def multi_host_special_machines(container, org)
    machine_configs = container.security_container_configs.for_machines.where(machine_id: org.machines)
    machine_configs.map(&:configurable)
  end

  def enabled_by_org?(container, org)
    org.required_containers.where(id: container.id).exists? ||
      org.security_container_configs.explicitly_enabled.where(security_container_id: container.id).exists?
  end
end
