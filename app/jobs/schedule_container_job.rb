# frozen_string_literal: true

require './lib/norad_exception_notifier'

class ScheduleContainerJob < ApplicationJob
  include NoradExceptionNotifier

  queue_as :swarm_scheduling

  rescue_from(StandardError) do |exception|
    notify_airbrake(exception)

    Resque.logger.error "Exception while starting job: #{exception.inspect}" if defined?(Resque)
    Resque.logger.error exception.backtrace.to_s if defined?(Resque)
    job_info = @arguments.first
    ActiveRecord::Base.transaction do
      job_info[:targets].each { |target| ResultGenerator.new(target[:assessment_global_id]).create_errored_api_result! }
    end
  end

  after_perform do |job|
    job_info = job.arguments.first
    ActiveRecord::Base.transaction do
      job_info[:targets].each do |target|
        assessment = target[:assessment_global_id]
        assessment.start!
      end
    end
  end

  # Create a canonical name for this container. That is, the SHA1 hash of the
  # sorted assessment IDs, joined by colons.
  def noradjob_assessment_id(targets)
    assessment_ids = targets.map { |t| t[:assessment_global_id].identifier }
    job_assessment_id =
      if assessment_ids.length == 1
        assessment_ids.first
      else
        OpenSSL::Digest::SHA1.hexdigest(assessment_ids.sort.join)
      end

    "noradjob_#{job_assessment_id}"
  end

  def perform(job_info)
    DockerSwarm.queue_container(
      noradjob_assessment_id(job_info[:targets]),
      GenericContainer.new(security_container_id: job_info[:container_id]),
      job_info[:args],
      job_info[:targets].map { |t| t.except(:assessment_global_id) },
      job_info[:container_secret],
      relay_secret: job_info[:relay_secret],
      relay_queue: job_info[:relay_queue],
      relay_exchange: job_info[:relay_exchange]
    )
  end
end
