# frozen_string_literal: true

# == Schema Information
#
# Table name: local_authentication_records
#
#  id                       :integer          not null, primary key
#  password_digest          :string
#  password_reset_token     :string
#  password_reset_sent_at   :datetime
#  authentication_method_id :integer
#  email_confirmation_token :string
#  email_confirmed_at       :datetime
#  created_at               :datetime         not null
#  updated_at               :datetime         not null
#
# Indexes
#
#  index_local_authentication_records_on_authentication_method_id  (authentication_method_id) UNIQUE
#  index_local_authentication_records_on_email_confirmation_token  (email_confirmation_token) UNIQUE
#  index_local_authentication_records_on_email_confirmed_at        (email_confirmed_at)
#  index_local_authentication_records_on_password_reset_sent_at    (password_reset_sent_at)
#  index_local_authentication_records_on_password_reset_token      (password_reset_token) UNIQUE
#

class PasswordResetSerializer < ActiveModel::Serializer
  attributes :password_reset_token
end
