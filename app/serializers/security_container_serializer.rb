# frozen_string_literal: true

# == Schema Information
#
# Table name: security_containers
#
#  id                          :integer          not null, primary key
#  name                        :string           not null
#  category                    :integer          default("whitebox"), not null
#  prog_args                   :string           not null
#  default_config              :jsonb
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#  multi_host                  :boolean          default(FALSE), not null
#  test_types                  :string           default([]), not null, is an Array
#  configurable                :boolean          default(FALSE), not null
#  application_type_id         :integer
#  help_url                    :string
#  security_test_repository_id :integer          not null
#
# Indexes
#
#  index_security_containers_on_application_type_id          (application_type_id)
#  index_security_containers_on_s_t_r_id_and_name            (security_test_repository_id,name) UNIQUE
#  index_security_containers_on_security_test_repository_id  (security_test_repository_id)
#
# Foreign Keys
#
#  fk_rails_6a0ccb7d5f  (application_type_id => application_types.id)
#  fk_rails_c5578062aa  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#

class SecurityContainerSerializer < ActiveModel::Serializer
  attributes :id, :name, :prog_args, :default_config, :category, :configurable, :help_url, :test_types,
             :security_test_repository_id, :multi_host, :full_path, :created_at, :updated_at
  belongs_to :application_type
  belongs_to :security_test_repository
end
