# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_scan_schedules
#
#  id         :integer          not null, primary key
#  machine_id :integer          not null
#  period     :integer          default("daily"), not null
#  at         :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_machine_scan_schedules_on_machine_id  (machine_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6b47f8c66a  (machine_id => machines.id) ON DELETE => cascade
#

class MachineScanScheduleSerializer < ActiveModel::Serializer
  attributes :id, :period, :at, :machine_id, :created_at, :updated_at
end
