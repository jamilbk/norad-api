# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_configurations
#
#  id                         :integer          not null, primary key
#  organization_id            :integer          not null
#  auto_approve_docker_relays :boolean          default(FALSE), not null
#  created_at                 :datetime         not null
#  updated_at                 :datetime         not null
#  use_relay_ssh_key          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_organization_configurations_on_organization_id  (organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_2555d2097f  (organization_id => organizations.id) ON DELETE => cascade
#

class OrganizationConfigurationSerializer < ActiveModel::Serializer
  attributes :id, :auto_approve_docker_relays, :organization_id, :use_relay_ssh_key
end
