# frozen_string_literal: true

# XXX This file should only be run manually. Don't include it as part of a deploy pipeline
path =
  if Rails.env == 'production'
    File.join(Rails.root, 'db', 'seeds', 'application_types.yml')
  else
    File.join(Rails.root, 'db', 'seeds', 'short_list_of_application_types.yml')
  end

YAML.load_file(path).each do |name, service_info|
  service_info.each do |port, transport_protocols|
    transport_protocols.each do |tp|
      next unless %w[tcp udp].include? tp
      ApplicationType.where(port: port, name: name, transport_protocol: tp).first_or_create!
    end
  end
end
