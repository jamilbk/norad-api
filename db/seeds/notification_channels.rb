# frozen_string_literal: true

# Seed notification channels
ActiveRecord::Base.transaction do
  Organization.find_each do |org|
    NOTIFICATIONS.each_key { |k| org.notification_channels.where(event: k).first_or_create! }
  end
end
