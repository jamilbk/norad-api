class CreateOrganizations < ActiveRecord::Migration[4.2]
  def change
    create_table :organizations do |t|
      t.string :uid, null: false
      t.string :slug, null: false

      t.timestamps null: false
    end
    add_index :organizations, :slug, unique: true
    add_index :organizations, :uid, unique: true
  end
end
