class CreateSecurityTestRepositories < ActiveRecord::Migration[5.0]
  REGISTRY_HOST = 'norad-registry.cisco.com:5000'.freeze

  def change
    create_table :security_test_repositories do |t|
      t.string :host
      t.string :name, null: false
      t.boolean :public, default: false
      t.string :username
      t.string :password_encrypted
      t.boolean :official, default: false
      t.timestamps
    end
    add_index :security_test_repositories, :host
    migrate_security_containers
    update_existing_security_containers
  end

  private

  def migrate_security_containers
    add_reference :security_containers, :security_test_repository, foreign_key: true
    remove_index :security_containers, :name
    add_index :security_containers,
              %i[security_test_repository_id name],
              unique: true,
              name: :index_security_containers_on_s_t_r_id_and_name
  end

  # At this point in the migration, assuming prod data, every container in the system is an "official"
  # one, and is prefixed with "norad-registry.cisco.com:5000"
  def insert_and_update_sql
    <<~SQL
      WITH repository AS (
        INSERT INTO security_test_repositories (host, name, public, official, created_at, updated_at)
        VALUES (
          '#{REGISTRY_HOST}',
          'Norad',
          't',
          't',
          'now()',
          'now()'
        )
        RETURNING id
      )

      UPDATE security_containers
      SET security_test_repository_id = (SELECT repository.id from repository),
          name = replace(name, '#{REGISTRY_HOST}/', '');
    SQL
  end

  def prefix_registry_host_sql
    <<~SQL
      UPDATE security_containers
      SET name = '#{REGISTRY_HOST}/' || name;
    SQL
  end

  def update_existing_security_containers
    reversible do |direction|
      direction.up { execute insert_and_update_sql }
      direction.down { execute prefix_registry_host_sql }
    end
  end
end
