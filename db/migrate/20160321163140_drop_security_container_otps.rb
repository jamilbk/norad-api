class DropSecurityContainerOtps < ActiveRecord::Migration[4.2]
  def up
    drop_table :security_container_otps
  end

  def down
    create_table :security_container_otps do |t|
      t.string :secret
      t.references :assessment, polymorphic: true

      t.timestamps null: false
    end
    add_index :security_container_otps, :secret, unique: true
    add_index(:security_container_otps,
              [:assessment_type, :assessment_id],
              name: 'index_security_container_otps_on_assessment_type_and_id'
             )
  end
end
