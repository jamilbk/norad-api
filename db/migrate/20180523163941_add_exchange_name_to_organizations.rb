class AddExchangeNameToOrganizations < ActiveRecord::Migration[5.0]
  def up
    add_column :organizations, :exchange_name, :uuid
    execute 'CREATE EXTENSION IF NOT EXISTS "uuid-ossp"'
    insert_uuids
    add_index :organizations, :exchange_name, unique: true
    change_column_null :organizations, :exchange_name, false
  end

  def down
    remove_column :organizations, :exchange_name
  end

  private

  def insert_uuids
    execute <<~SQL
      UPDATE organizations
      SET exchange_name = uuid_generate_v4()
    SQL
  end
end
