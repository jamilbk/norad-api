class AddUniqueConstraintToPortForServices < ActiveRecord::Migration[4.2]
  def change
    add_index :services, [:machine_id, :port], unique: true
  end
end
