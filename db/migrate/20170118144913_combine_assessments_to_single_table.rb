class CombineAssessmentsToSingleTable < ActiveRecord::Migration[5.0]
  Assessment.class_eval do
    private

    # We need to use the existing identifier
    def generate_identifier
      yield
    end
  end

  class BlackBoxAssessment < Assessment
    # Don't look into the assessment table for the BBA info
    self.table_name = 'black_box_assessments'
  end


  def up
    set_up_tables
    migrate_bba_data
    drop_old_table_info
  end

  def down
  end

  private

  def set_up_tables
    rename_wba_table_to_assessments
    add_type_column_to_assessments
    rename_wba_id_column_on_results_table
  end

  def rename_wba_table_to_assessments
    rename_table :white_box_assessments, :assessments
  end

  def rename_assessment_indexes
    rename_index :assessments, 'index_white_box_assessments_on_docker_command_id', 'index_assessments_on_docker_command_id'
    rename_index :assessments, 'index_white_box_assessments_on_identifier', 'index_assessments_on_identifier'
    rename_index :assessments, 'index_white_box_assessments_on_machine_id', 'index_assessments_on_machine_id'
    rename_index :assessments, 'index_white_box_assessments_on_security_container_id', 'index_assessments_on_security_container_id'
    rename_index :assessments, 'index_white_box_assessments_on_security_container_secret_id', 'index_assessments_on_security_container_secret_id'
    rename_index :assessments, 'index_white_box_assessments_on_service_id', 'index_assessments_on_service_id'
  end

  def add_type_column_to_assessments
    # Populate the initial columns with the proper type...
    add_column :assessments, :type, :string, index: true, default: 'WhiteBoxAssessment', null: false
    # Then remove the default
    change_column_default :assessments, :type, nil
  end

  def rename_wba_id_column_on_results_table
    rename_column :results, :white_box_assessment_id, :assessment_id
  end

  def migrate_bba_data
    BlackBoxAssessment.all.each do |bba|
      ActiveRecord::Base.transaction do
        old_id = bba.id
        assessment = Assessment.new(bba.attributes.without('id').merge(type: 'BlackBoxAssessment'))
        # Some old assessments don't comply with our current validations, so validations need to be
        # skipped
        assessment.save!(validate: false)
        Result.where(black_box_assessment_id: old_id).update_all(assessment_id: assessment.id)
      end
    end
  end

  def drop_old_table_info
    remove_column :results, :black_box_assessment_id
    drop_table :black_box_assessments
  end
end
