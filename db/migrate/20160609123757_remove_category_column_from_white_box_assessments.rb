class RemoveCategoryColumnFromWhiteBoxAssessments < ActiveRecord::Migration[4.2]
  def up
    change_table :white_box_assessments do |t|
      t.remove :category
    end
  end

  def down
    change_table :white_box_assessments do |t|
      t.integer :category, default: 0, null: false
    end
  end
end
