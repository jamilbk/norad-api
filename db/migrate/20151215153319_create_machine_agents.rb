class CreateMachineAgents < ActiveRecord::Migration[4.2]
  def change
    create_table :machine_agents do |t|
      t.references :machine, index: true, null: false
      t.text :public_key, null: false
      t.string :queue_name, null: false
      t.integer :state, null: false, default: 0
      t.timestamp :last_heartbeat, null: false
      t.boolean :verified, null: false, default: false

      t.timestamps null: false
    end
    add_index :machine_agents, :queue_name, unique: true
    add_foreign_key :machine_agents, :machines, on_delete: :cascade
  end
end
