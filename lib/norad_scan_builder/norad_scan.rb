# frozen_string_literal: true

module NoradScanBuilder
  class NoradScan
    attr_reader :target, :args, :dc, :container, :secret_id

    def initialize(container, secret_id, command, opts = {})
      @container = container
      @secret_id = secret_id
      @dc = command
      post_initialize opts
    end

    def post_initialize(_opts)
      raise NotImplementedError
    end

    def to_h
      {
        targets: targets_array, # Beacon expects an Array
        args: args.to_a
      }
    end

    def empty?
      args.to_a.empty? || targets_array.empty? || targets_array.any? { |t| t.blank? || t[:id].nil? }
    end

    private

    def targets_array
      [target].flatten.map(&:to_h)
    end
  end
end
