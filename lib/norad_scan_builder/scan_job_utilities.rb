# frozen_string_literal: true

module NoradScanBuilder
  module ScanJobUtilities
    include NoradRelayHelpers

    def to_h
      relay_opts(organization).merge(
        container_secret: secret.secret,
        container_id: container_id
      )
    end
  end
end
