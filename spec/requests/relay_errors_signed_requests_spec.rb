# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe 'Relay Error signed request communication', type: :request do
  include NoradControllerTestHelpers

  before :each do
    @keypair = OpenSSL::PKey::RSA.new(4096)
    @relay = create :docker_relay, public_key: Base64.encode64(@keypair.public_key.to_pem).tr("\n", '')
  end

  def create_error(sig)
    signed_request(:post, v1_docker_relay_errors_path(@relay), @payload, sig)
  end

  def remove_error(sig)
    signed_request(:delete, v1_docker_relay_error_path(@relay, @error), @payload, sig)
  end

  describe 'POST #create' do
    before(:each) do
      @payload = {
        timestamp: Time.now.to_f.to_s,
        docker_relay_id: @relay.to_param,
        relay_error: { type: 'RelayQueueError' }
      }
    end

    it 'requires the payload to be signed by the relay private key' do
      create_error('junk')
      expect(response).to have_http_status :unauthorized
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      create_error(sig)
      expect(response).to have_http_status :no_content
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @error = create :relay_queue_error, organization: @relay.organization
      @payload = {
        timestamp: Time.now.to_f.to_s,
        docker_relay_id: @relay.to_param
      }
    end

    it 'requires the payload to be signed by the relay private key' do
      remove_error('junk')
      expect(response).to have_http_status :unauthorized
      sig = Base64.encode64(@keypair.sign(OpenSSL::Digest::SHA256.new, @payload.to_json)).tr("\n", '')
      remove_error(sig)
      expect(response).to have_http_status :no_content
    end
  end
end
