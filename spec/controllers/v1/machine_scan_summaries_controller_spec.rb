# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::MachineScanSummariesController, type: :controller do
  let(:org) { @_current_user.organizations.first }

  def create_results_for_assessment(assessment)
    %i[pass fail error warn info].each do |status|
      create :result, status: status, assessment: assessment
    end
  end

  describe 'GET #show' do
    let(:machine) { create :machine, organization: org }
    let(:dc) { create :docker_command, commandable: machine }
    let(:assessment) { create :white_box_assessment, machine: machine, docker_command: dc }
    let(:summary) { MachineScanSummary.new(machine: machine) }

    before :each do
      create_results_for_assessment(assessment)
    end

    def show
      norad_get :show, machine_id: machine.to_param
    end

    it 'calls the authorizer' do
      allow(MachineScanSummary).to receive(:new).and_return(summary)
      expect(subject).to receive(:authorize_action_for).with(summary, in: org)
      show
    end

    context 'response object' do
      before(:each) { show }

      it 'responds with success' do
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_scan_summary')
      end

      it 'includes assessments_summary' do
        expect(response_body['response']['assessments_summary']).to be_present
      end
    end
  end
end
