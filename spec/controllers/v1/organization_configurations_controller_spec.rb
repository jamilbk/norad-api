# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::OrganizationConfigurationsController, type: :controller do
  describe 'GET #show' do
    before :each do
      @organization = create :organization
      @config = @organization.configuration
      @admin_user = create :user
    end

    it 'does not expose the configuration to an unprivileged user' do
      norad_get :show, id: @config.to_param
      expect(response.status).to be(403)
    end

    it 'exposes the configurtion to an admin user' do
      allow(controller).to receive(:current_user).and_return(@admin_user)
      @admin_user.add_role :organization_admin, @organization
      norad_get :show, id: @config.to_param
      expect(response.status).to be(200)
      expect(response).to match_response_schema('organization_configuration')
    end
  end

  describe 'PUT #update' do
    before :each do
      @org = create :organization
      @config = @org.configuration
    end

    context 'with valid params' do
      let(:new_attributes) do
        { auto_approve_docker_relays: true }
      end

      it 'enforces access control for the update action' do
        norad_put :update, id: @config.to_param, organization_configuration: new_attributes
        expect(response.status).to be(403)
      end

      it 'updates the requested configuration' do
        @admin_user = create(:user).tap { |u| u.add_role :organization_admin, @org }
        expect(@config.auto_approve_docker_relays).to_not eq(new_attributes[:auto_approve_docker_relays])
        allow(controller).to receive(:current_user).and_return(@admin_user)
        norad_put :update, id: @config.to_param, organization_configuration: new_attributes
        @config.reload
        expect(@config.auto_approve_docker_relays).to eq new_attributes[:auto_approve_docker_relays]
        expect(response).to match_response_schema('organization_configuration')
      end
    end
  end
end
