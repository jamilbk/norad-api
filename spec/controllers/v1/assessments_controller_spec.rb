# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::AssessmentsController, type: :controller do
  before :each do
    @org1 = create :organization
    @org2 = create :organization
    @machine = create :machine, organization: @org1
  end

  describe 'GET #index' do
    def expect_success
      expect_response_success
      expect_schema_success
      expect(response_body['response'].size).to eq(assessments.size)
    end

    def expect_response_success
      expect(response).to have_http_status(:success)
    end

    def expect_schema_success
      expect(response).to match_response_schema('assessments')
    end

    context 'scoped to a docker_command' do
      let(:docker_command) { create :docker_command, commandable: @org1 }
      let(:security_container) { docker_command.security_containers.first }
      let!(:assessments) do
        Array.new(2) { create :white_box_assessment, machine: @machine, docker_command: docker_command }
      end

      def norad_get_index(opts = {})
        norad_get :index, opts.merge(docker_command_id: docker_command.to_param)
      end

      context 'as an admin' do
        before :each do
          @_current_user.add_role :organization_admin, @org1
        end

        it 'exposes all enforced assessments for a docker_command' do
          norad_get_index
          expect_success
        end
      end

      context 'as an admin from another organization' do
        before :each do
          @_current_user.add_role :organization_admin, @org2
        end

        it 'and fail' do
          norad_get_index
          expect(response.status).to eq(403)
        end
      end

      context 'as a reader' do
        before :each do
          @_current_user.add_role :organization_reader, @org1
        end

        it 'exposes all assessments for a machine' do
          norad_get_index
          expect_success
        end
      end

      context 'as a reader from another organization' do
        before :each do
          @_current_user.add_role :organization_reader, @org2
        end

        it 'and fail' do
          norad_get_index
          expect(response.status).to eq(403)
        end
      end
    end

    context 'scoped to a machine' do
      let!(:assessments) { Array.new(2) { create :white_box_assessment, machine: @machine } }

      context 'as an admin' do
        before :each do
          @_current_user.add_role :organization_admin, @org1
        end

        it 'exposes all assessments for a machine' do
          norad_get :index, machine_id: @machine.to_param, limit: 'all'
          expect_success
        end

        context 'when limit is not provided' do
          it 'exposes assessments sorted by most recent docker_command_id' do
            norad_get :index, machine_id: @machine.to_param, limit: ''
            expect_success
          end
        end

        context 'when scoped to a docker_command_id' do
          let(:dc1) { create :docker_command, commandable: @machine }
          let(:dc2) { create :docker_command, commandable: @machine }
          let!(:assessments) { [create(:white_box_assessment, docker_command: dc1, machine: @machine)] }
          let!(:excluded_assessments) { [create(:white_box_assessment, docker_command: dc2, machine: @machine)] }

          it 'scopes the assessments by machine and docker_command_id' do
            norad_get :index, machine_id: @machine.to_param, docker_command_id: dc1.to_param
            expect(@machine.assessments.size).to eq 2
            expect_success
            expect(response_body['response'].map { |a| a['identifier'] })
              .to contain_exactly(assessments.first.identifier)
          end
        end
      end

      context 'as an admin from another organization' do
        before :each do
          @_current_user.add_role :organization_admin, @org2
        end

        it 'and fail' do
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end
      end

      context 'as a reader' do
        before :each do
          @_current_user.add_role :organization_reader, @org1
        end

        it 'exposes all assessments for a machine' do
          norad_get :index, machine_id: @machine.to_param, limit: 'all'
          expect_success
        end

        context 'when limit is not provided' do
          it 'exposes assessments sorted by most recent docker_command_id' do
            norad_get :index, machine_id: @machine.to_param, limit: ''
            expect_success
          end
        end
      end

      context 'as a reader from another organization' do
        before :each do
          @_current_user.add_role :organization_reader, @org2
        end

        it 'and fail' do
          norad_get :index, machine_id: @machine.to_param
          expect(response.status).to eq(403)
        end
      end
    end
  end

  describe 'GET #latest' do
    before :each do
      @wba = create :white_box_assessment, machine: @machine
    end

    context 'as an admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
      end

      it 'includes results in the response' do
        Array.new(3) do
          @wba.results << create(:result)
        end
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response_body['response'].first['results']).to_not be_empty
        expect(response).to match_response_schema('assessments')
      end
    end

    context 'as an admin from another organization' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'and fail' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'as a reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessments')
      end
    end

    context 'as a reader from another organization' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'exposes an individual assessment' do
        norad_get :latest, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @wba = create :white_box_assessment, machine: @machine
    end

    context 'as an admin' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessment')
      end
    end

    context 'as an admin from another organization' do
      before :each do
        @_current_user.add_role :organization_admin, @org2
      end

      it 'and fail' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(403)
      end
    end

    context 'as a reader' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('assessment')
      end
    end

    context 'as a reader from another organization' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'exposes an individual assessment' do
        norad_get :show, id: @wba.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'PUT #update' do
    let(:assessment) { create :assessment }

    before(:each) do
      allow(controller).to receive(:require_container_secret_signature).and_return(true)
    end

    after(:each) do
      allow(controller).to receive(:require_container_secret_signature).and_call_original
    end

    def expect_success
      norad_put :update, req_params.merge(id: assessment.to_param)
      expect(assessment.reload).to be_complete
      expect(response).to have_http_status :no_content
    end

    def expect_not_modified
      norad_put :update, req_params.merge(id: assessment.to_param)
      expect(assessment.reload).not_to be_complete
      expect(response).to have_http_status :not_modified
    end

    context 'when state param is "complete"' do
      let(:req_params) { { assessment: { state: :complete } } }

      it 'changes state to complete' do
        expect_success
      end
    end

    context 'when state param is not "complete"' do
      let(:req_params) { { assessment: { state: :anything } } }

      it 'does not change state to complete' do
        expect_not_modified
      end
    end

    context 'when state param is not provided' do
      let(:req_params) { { assessment: { missing_state: :anything } } }

      it 'does not change state' do
        expect_not_modified
      end
    end

    context 'when assessment param is blank' do
      let(:req_params) { { assessment: {} } }

      it 'raises ActionController::ParameterMissing' do
        expect do
          norad_put :update, req_params.merge(id: assessment.to_param)
        end.to raise_exception ActionController::ParameterMissing
      end
    end
  end
end
