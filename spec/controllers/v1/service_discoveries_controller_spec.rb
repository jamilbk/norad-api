# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::ServiceDiscoveriesController, type: :controller do
  describe 'GET #index' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine = create :machine, organization: @org1
      create :service_discovery, machine: @machine
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of service_discoveries for an organization' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service_discoveries')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'can read the details of a specific service_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(200)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific service_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :index, machine_id: @machine.to_param
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine = create :machine, organization: @org1
      @service_discovery = create :service_discovery, machine: @machine
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can read the details of a specific service_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @service_discovery.to_param
        expect(response.status).to eq(200)
        expect(response).to match_response_schema('service_discovery')
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot read the details of a specific service_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        norad_get :show, id: @service_discovery
        expect(response.status).to eq(200)
      end
    end

    context 'a user outside the organization' do
      it 'cannot read the details of a specific service_discovery' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        norad_get :show, id: @service_discovery
        expect(response.status).to eq(403)
      end
    end
  end

  describe 'POST #create' do
    before :each do
      @org1 = create :organization
      @org2 = create :organization
      @machine = create :machine, organization: @org1
    end

    context 'an organization admin,' do
      before :each do
        @_current_user.add_role :organization_admin, @org1
      end

      it 'can create new service_discovery for machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(true)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@machine.service_discoveries.first).to be(nil)
        norad_post :create, machine_id: @machine.to_param
        expect(@machine.service_discoveries.first).not_to be(nil)
        expect(response.status).to eq(200)
      end
    end

    context 'an organization reader,' do
      before :each do
        @_current_user.add_role :organization_reader, @org1
      end

      it 'cannot create new service_discovery for machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(true)
        expect(@machine.service_discoveries.first).to be(nil)
        norad_post :create, machine_id: @machine.to_param
        expect(@machine.service_discoveries.first).to be(nil)
        expect(response.status).to eq(403)
      end
    end

    context 'an organization outsider with admin in other org,' do
      before :each do
        @_current_user.add_role :organization_reader, @org2
      end

      it 'cannot create new service_discovery for machine' do
        expect(@_current_user.has_role?(:organization_admin, @org1)).to eq(false)
        expect(@_current_user.has_role?(:organization_reader, @org1)).to eq(false)
        expect(@machine.service_discoveries.first).to be(nil)
        norad_post :create, machine_id: @machine.to_param
        expect(@machine.service_discoveries.first).to be(nil)
        expect(response.status).to eq(403)
      end
    end
  end
end
