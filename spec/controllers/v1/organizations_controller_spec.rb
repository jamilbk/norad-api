# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::OrganizationsController, type: :controller do
  describe 'GET #index' do
    context 'for readers' do
      before :each do
        create :organization
        @default_org = Organization.find "#{@_current_user.uid}-org-default"
        @read_org = create :organization
        @_current_user.add_role :organization_reader, @read_org
        @_current_user.remove_role :organization_admin, @default_org
        @_current_user.add_role :organization_reader, @default_org
      end

      it 'renders an array of organizations matching the reader schema' do
        norad_get :index, {}
        expect(response).to match_response_schema('organizations_reader_with_null_primary_relay')
      end

      it 'exposes organizations where user has admin or reader role' do
        norad_get :index, {}
        returned_ids = response_body['response'].map { |o| o['id'] }
        expected_ids = [@read_org.id, @default_org.id]
        expect(returned_ids).to match_array(expected_ids)
      end

      it 'does not have any assessment summary stats' do
        norad_get :index, {}
        response_body['response'].each do |o|
          expect(o['machine_assessment_summary']).to eq({})
        end
      end
    end

    context 'for admins' do
      before :each do
        @my_org = create :organization
        @_current_user.add_role :organization_admin, @my_org
      end

      it 'renders an array of organizations matching the admin schema' do
        norad_get :index, {}
        expect(response).to match_response_schema('organizations_admin_with_null_primary_relay')
      end

      it 'only returns org tokens for orgs that the current user has the admin role' do
        @other_org = create :organization
        @_current_user.add_role :organization_reader, @other_org
        norad_get :index, {}
        resp_my_org = response_body['response'].find { |o| o['id'].to_i == @my_org.id }
        resp_other_org = response_body['response'].find { |o| o['id'].to_i == @other_org.id }

        expect(resp_my_org['token']).to eq @my_org.token
        expect(resp_other_org['token']).to be nil
      end
    end
  end

  describe 'GET #show' do
    before :each do
      @organization = create :organization
      @admin_user = create :user
    end

    it 'does not expose the organization to an unprivileged user' do
      norad_get :show, id: @organization.to_param
      expect(response.status).to be(403)
    end

    it 'exposes the organization to an admin user' do
      allow(controller).to receive(:current_user).and_return(@admin_user)
      @admin_user.add_role :organization_admin, @organization
      norad_get :show, id: @organization.to_param
      expect(response_body['response']['token']).to eq @organization.token
    end

    it 'conditionally includes assessment summary' do
      allow(controller).to receive(:current_user).and_return(@admin_user)
      @admin_user.add_role :organization_admin, @organization
      norad_get :show, id: @organization.to_param, include_assessment_summary: true
      expect(response_body['response']['machine_assessment_summary'].empty?).not_to be_truthy

      norad_get :show, id: @organization.to_param
      expect(response_body['response']['machine_assessment_summary'].empty?).to be_truthy
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      def create_organization
        norad_post :create, organization: attributes_for(:organization)
      end

      it 'allows anyone to create an organization' do
        expect { create_organization }.to change(Organization, :count).by(1)
      end

      it 'exposes the newly created organization' do
        create_organization
        expect(response).to match_response_schema('organization_admin_with_null_primary_relay')
      end

      it 'assigns the current session user to the role of admin for the organization' do
        standard_user = create :user
        allow(controller).to receive(:current_user).and_return(standard_user)
        create_organization
        organization = Organization.find(response_body['response']['id'])
        expect(standard_user.has_role?(:organization_admin, organization)).to be(true)
      end

      it 'create an org with the reserve word in the UID and fail' do
        attributes = attributes_for(:organization)
        attributes[:uid] = 'Organization Default'
        expect { norad_post :create, organization: attributes }.to change(Organization, :count).by(0)
        expect(response.status).to eq(422)
      end
    end
  end

  describe 'PUT #update' do
    before :each do
      @org = create :organization
    end

    context 'with valid params' do
      let(:new_attributes) do
        { uid: 'MY_NEW_TEST_ORG_UID' }
      end

      it 'enforces access control for the update action' do
        norad_put :update, id: @org.to_param, organization: new_attributes
        expect(response.status).to be(403)
      end

      it 'updates the requested organization' do
        @admin_user = create(:user).tap { |u| u.add_role :organization_admin, @org }
        expect(@org.uid).to_not eq new_attributes['uid']
        allow(controller).to receive(:current_user).and_return(@admin_user)
        norad_put :update, id: @org.to_param, organization: new_attributes
        @org.reload
        expect(@org.uid).to eq new_attributes[:uid]
        @org.reload
        expect(response_body['response']['id']).to eq @org.id
      end
    end

    context 'with invalid params' do
      it 'responds with an error message' do
        @admin_user = create(:user).tap { |u| u.add_role :organization_admin, @org }
        new_uid = ''
        allow(controller).to receive(:current_user).and_return(@admin_user)
        norad_put :update, id: @org.to_param, organization: { uid: new_uid }
        @org.reload
        expect(response.status).to eq(422)
        @org.reload
        expect(@org.uid).to_not eq new_uid
      end
    end
  end

  describe 'DELETE #destroy' do
    before :each do
      @org = create :organization
    end

    it 'enforces access control for the destroy action' do
      expect do
        norad_delete :destroy, id: @org.to_param
      end.to change(Organization, :count).by(0)
      expect { @org.reload }.to_not raise_exception
      expect(response.status).to be(403)
    end

    it 'destroys the requested @org' do
      @admin_user = create(:user).tap { |u| u.add_role :organization_admin, @org }
      allow(controller).to receive(:current_user).and_return(@admin_user)
      expect do
        norad_delete :destroy, id: @org.to_param
      end.to change(Organization, :count).by(-1)
      expect { @org.reload }.to raise_exception ActiveRecord::RecordNotFound
    end
  end
end
