# frozen_string_literal: true

require 'rails_helper'
require 'support/controller_helper'

RSpec.describe V1::ResultExportersController, type: :controller do
  include NoradControllerTestHelpers

  let(:org) { create :organization }
  let(:result) { create :result }

  shared_examples 'Result Exporters Controller' do
    before(:each) do
      @_current_user.add_role role, org
    end

    it 'returns a 400 bad request if result IDs span organizations' do
      m = create :machine, organization: org
      a1 = create :white_box_assessment, machine: m
      a2 = create :white_box_assessment
      r1 = create :result, assessment: a1
      r2 = create :result, assessment: a2

      expect { norad_post :create, result_ids: [r1.id, r2.id] }.to raise_error(ActionController::BadRequest)
      expect(a1.machine.organization.id).to_not eq(a2.machine.organization.id)
    end

    it 'retuns 201 Created for valid requests' do
      m = create :machine, organization: org
      a1 = create :white_box_assessment, machine: m
      a2 = create :white_box_assessment, machine: m
      r1 = create :result, assessment: a1
      r2 = create :result, assessment: a2

      norad_post :create, result_ids: [r1.id, r2.id]

      expect(response.status).to eq(201)
    end

    it 'schedules export jobs' do
      m = create :machine, organization: org
      a1 = create :white_box_assessment, machine: m
      a2 = create :white_box_assessment, machine: m
      r1 = create :result, assessment: a1
      r2 = create :result, assessment: a2
      q1 = create :jira_export_queue, organization: org
      q2 = create :jira_export_queue, organization: org
      allow(Organization).to receive(:find).and_return(org)
      allow(org).to receive(:result_export_queues).and_return([q1, q2])

      # ActionController converts integer params to strings
      expect(q1).to receive(:export).with([r1.id, r2.id].map(&:to_s))
      expect(q2).to receive(:export).with([r1.id, r2.id].map(&:to_s))

      norad_post :create, result_ids: [r1.id, r2.id]
    end
  end

  describe 'POST #create' do
    context 'for organization admins' do
      include_examples 'Result Exporters Controller' do
        let(:role) { :organization_admin }
      end
    end

    context 'for organization readers' do
      include_examples 'Result Exporters Controller' do
        let(:role) { :organization_reader }
      end
    end

    context 'for users outside of the organization' do
      it 'denies access for users without a role on the organization' do
        m = create :machine, organization: org
        a1 = create :white_box_assessment, machine: m
        a2 = create :white_box_assessment, machine: m
        r1 = create :result, assessment: a1
        r2 = create :result, assessment: a2

        norad_post :create, result_ids: [r1.id, r2.id]

        expect(response.status).to eq(403)
      end
    end
  end
end
