# frozen_string_literal: true

require 'rails_helper'

shared_examples_for 'a connectivity checks controller' do
  let!(:organization) { @_current_user.organizations.first }
  let(:machine) { create :machine, organization: organization }

  before(:each) do
    allow_any_instance_of(SshConnectivityCheck).to receive(:eligible).and_return(true)
  end

  describe 'GET #show' do
    let(:check) { create factory, machine: machine }

    context 'as an organization admin' do
      before(:each) do
        @_current_user.remove_role :organization_reader, organization
        @_current_user.add_role :organization_admin, organization
        @earlier = create factory, machine: machine, created_at: 1.day.ago
        @later = create factory, machine: machine
        @other = create factory
      end

      it 'fetches a single check' do
        norad_get :show, machine_id: machine.to_param, id: @earlier.to_param
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_connectivity_check')
      end

      it 'raises ActiveRecord::RecordNotFound when the check belongs to a different machine' do
        expect do
          norad_get :show, machine_id: machine.to_param, id: @other.to_param
        end.to raise_exception(ActiveRecord::RecordNotFound)
      end

      it 'fetches the latest check when id is "latest"' do
        norad_get :show, machine_id: machine.to_param, id: 'latest'
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_connectivity_check')
        expect(@later.created_at).to be > @earlier.created_at
        expect(response_body['response']['id']).to eq @later.id
      end
    end

    context 'as an organization reader' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.add_role :organization_reader, organization
      end

      it 'returns specified check' do
        norad_get :show, machine_id: machine.to_param, id: check.to_param
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_connectivity_check')
      end
    end

    context 'as a user outside the organization' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.remove_role :organization_reader, organization
      end

      it 'responds with forbidden' do
        norad_get :show, machine_id: machine.to_param, id: check.to_param
        expect(response).to have_http_status :forbidden
      end
    end
  end

  describe 'GET #index' do
    before(:each) do
      check = double
      allow(check).to receive(:id)
      allow(MachineError).to receive(:create_check).and_return(check)
      2.times { create factory, machine: machine }
    end

    context 'as an organization admin' do
      before(:each) do
        @_current_user.remove_role :organization_reader, organization
        @_current_user.add_role :organization_admin, organization
      end

      it 'lists all connectivity checks scoped to machine' do
        norad_get :index, machine_id: machine.to_param
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_connectivity_checks')
        expect(response_body['response'].size).to eq 2
      end
    end

    context 'as an organization reader' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.add_role :organization_reader, organization
      end

      it 'lists all checks scoped to a machine' do
        norad_get :index, machine_id: machine.to_param
        expect(response).to have_http_status :success
        expect(response).to match_response_schema('machine_connectivity_checks')
        expect(response_body['response'].size).to eq 2
      end
    end

    context 'as a user outside the organization' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.remove_role :organization_reader, organization
      end

      it 'responds with forbidden' do
        norad_get :index, machine_id: machine.to_param
        expect(response).to have_http_status :forbidden
      end
    end
  end

  describe 'POST #create' do
    context 'as an organization admin' do
      before(:each) do
        @_current_user.remove_role :organization_reader, organization
        @_current_user.add_role :organization_admin, organization
      end

      context 'with valid params' do
        it 'responds with success' do
          expect(ScheduleMachineConnectivityCheckJob).to receive(:perform_later)
            .with(kind_of(Integer), {})

          norad_post :create, machine_id: machine.to_param

          expect(response).to have_http_status :success
          expect(response).to match_response_schema('machine_connectivity_check')
        end
      end

      context 'when validation fails' do
        before(:each) do
          allow_any_instance_of(MachineConnectivityCheck).to receive(:save).and_return(false)
        end

        it 'responds with unprocessable entity' do
          norad_post :create, machine_id: machine.to_param
          expect(response).to have_http_status :unprocessable_entity
        end
      end
    end

    context 'as an organization reader' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.add_role :organization_reader, organization
      end

      it 'responds with forbidden' do
        norad_post :create, machine_id: machine.to_param, connectivity_check: { foo: :bar }
        expect(response).to have_http_status :forbidden
      end
    end

    context 'as a user outside the organization' do
      before(:each) do
        @_current_user.remove_role :organization_admin, organization
        @_current_user.remove_role :organization_reader, organization
      end

      it 'responds with forbidden' do
        norad_post :create, machine_id: machine.to_param, connectivity_check: { foo: :bar }
        expect(response).to have_http_status :forbidden
      end
    end
  end

  describe 'PUT #update' do
    let(:check) { create factory, machine: machine }

    before(:each) do
      allow(controller).to receive(:require_container_secret_signature).and_return(true)
    end

    after(:each) do
      allow(controller).to receive(:require_container_secret_signature).and_call_original
    end

    context 'as a valid relay' do
      context 'with valid params' do
        let(:valid_params) { { status: :passing } }

        it 'responds with success' do
          norad_put :update, id: check.id, connectivity_check: valid_params
          expect(response).to have_http_status :success
        end
      end

      context 'with invalid params' do
        it 'raises ActionController::ParameterMissing' do
          expect do
            norad_put :update, id: check.id, foo: :bar
          end.to raise_exception(ActionController::ParameterMissing)
        end
      end

      context 'when validation fails' do
        let(:valid_params) { { status: :passing } }

        before(:each) do
          allow_any_instance_of(MachineConnectivityCheck).to receive(:update).and_return(false)
        end

        it 'responds with unprocessable entity' do
          norad_put :update, id: check.id, connectivity_check: valid_params
          expect(response).to have_http_status :unprocessable_entity
        end
      end
    end
  end
end
