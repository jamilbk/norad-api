# frozen_string_literal: true

require 'rails_helper'

RSpec.describe V1::UsersController, type: :controller do
  describe 'GET #show' do
    it 'exposes self user' do
      norad_get :show, id: @_current_user.to_param
      expect(response).to match_response_schema('user_with_token')
    end

    it 'responds with error message while showing other user' do
      # Logged in user is @_current_user
      user = create :user
      norad_get :show, id: user.to_param
      expect(response.status).to eq(403)
    end
  end

  describe 'POST #authenticate' do
    context 'reverse proxy auth' do
      before :each do
        @user_to_auth = create(:user_with_sso).reload
        ENV['NORAD_UI_SECRET'] = '1234'
      end

      it 'returns an unauthenticated error if the request does not provide the shared secret' do
        norad_post :authenticate, id: @user_to_auth.to_param
        expect(response.status).to eq(401)
      end

      it 'returns an unauthenticated error if the request provides an incorrect secret' do
        request.headers['HTTP-NORAD-UI-SECRET'] = '4321'
        norad_post :authenticate, id: @user_to_auth.to_param
        expect(response.status).to eq(401)
      end

      it 'returns an unauthenticated error if the secret is not set in the environment' do
        ENV.delete 'NORAD_UI_SECRET'
        request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
        norad_post :authenticate, id: @user_to_auth.to_param
        expect(response.status).to eq(401)
      end

      it 'responds with the user and API token when valid secret is provided' do
        request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
        norad_post :authenticate, id: @user_to_auth.to_param
        expect(response_body['response']['api_token']).to eq @user_to_auth.api_token.value
      end

      context 'NEW_USERS_DISABLED flag is set to true' do
        before { ENV['NEW_USERS_DISABLED'] = 'true' }
        after { ENV['NEW_USERS_DISABLED'] = 'false' }

        it 'responds with a forbidden error on new user sign in' do
          request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
          norad_post :authenticate, id: 'noradtest'

          expect(response.status).to eq(403)
        end

        it 'allows preexisting users to sign in' do
          user = create :user_with_sso
          request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
          norad_post :authenticate, id: user.uid

          expect(response.status).to eq(200)
          expect(response_body['response']['email']).to eq(user.email)
        end

        it 'creates a new user if uid is whitelisted' do
          stub_const(
            'AuthenticationMethod::LOGIN_WHITELIST_CONFIG',
            Rails.root.join('spec', 'support', 'new_user_whitelist.yml')
          )
          request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
          norad_post :authenticate, id: 'noradtest'

          expect(response.status).to eq 200
          expect(response_body['response']['email']).to eq 'noradtest@cisco.com'
        end
      end

      it 'creates a new user if one is not found' do
        request.headers['HTTP-NORAD-UI-SECRET'] = '1234'
        norad_post :authenticate, id: 'noradtest'
        expect(response.status).to eq 200
        expect(response_body['response']['email']).to eq 'noradtest@cisco.com'
      end
    end

    context 'local auth' do
      let(:valid_auth_params) { { user: { email: @user.email, password: 'MyPassword' } } }
      let(:invalid_auth_params) { { user: { email: @user.email, password: 'badpassword' } } }

      before :each do
        mock_auth_type :local
        @user = create(:user_with_password).reload
        @user.confirm!
      end

      after(:each) { mock_auth_type :reverse_proxy }

      it 'authenticates with valid credentials' do
        norad_post :authenticate, valid_auth_params
        expect(response.status).to eq 200
        expect(response_body['response']['email']).to eq @user.email
      end

      it 'returns unauthenticated error for invalid password' do
        norad_post :authenticate, invalid_auth_params
        expect(response.status).to eq(401)
      end
    end
  end

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { firstname: 'MYNEWTESTFIRSTNAME' }
      end

      it 'updates self user' do
        expect(@_current_user.firstname).to_not eq new_attributes['firstname']
        norad_put :update, id: @_current_user.to_param, user: new_attributes
        expect(response.status).to eq(200)
        @_current_user.reload
        expect(@_current_user.firstname).to eq new_attributes[:firstname]
      end

      it 'responds with error message while updating other user' do
        user = create :user
        expect(user.firstname).to_not eq new_attributes['firstname']
        norad_put :update, id: user.to_param, user: new_attributes
        expect(response.status).to eq(403)
        user.reload
        expect(user.firstname).to_not eq new_attributes[:firstname]
      end
    end

    context 'with invalid params' do
      let(:new_attributes) do
        {
          firstname: '123456789',
          lastname: '123456789'
        }
      end

      it 'renders errors for user' do
        expect(@_current_user.firstname).to_not eq new_attributes['firstname']
        norad_put :update, id: @_current_user.to_param, user: new_attributes
        expect(response.status).to eq(422)
        expect(JSON.parse(response.body)['errors']['firstname'].first).to eq 'is invalid'
        expect(JSON.parse(response.body)['errors']['lastname'].first).to eq 'is invalid'
        @_current_user.reload
        expect(@_current_user.firstname).to_not eq new_attributes['firstname']
      end
    end
  end

  describe 'POST #create' do
    before(:each) { mock_auth_type :local }
    after(:each) { mock_auth_type :reverse_proxy }

    context 'with valid params' do
      let(:user_params) do
        {
          email: 'validemail@norad.dev',
          local_authentication_method_attributes: {
            local_authentication_record_attributes: {
              password: 'password',
              password_confirmation: 'password'
            }
          }
        }
      end

      it 'should create user' do
        expect { norad_post :create, user: user_params }.to change(User, :count).by(1)
        expect(response.status).to eq 200
      end
    end

    context 'with invalid params' do
      let(:user_params) do
        {
          email: 'invalid',
          local_authentication_method_attributes: {
            local_authentication_record_attributes: {
              password: 'password',
              password_confirmation: 'password1'
            }
          }
        }
      end

      it 'should not create user' do
        expect { norad_post :create, user: user_params }.not_to change(User, :count)
        expect(response.status).to eq 422
        errors = JSON.parse(response.body)['errors']
        expect(errors['email'].first).to eq 'is invalid'
        expect(errors['local_authentication_method.local_authentication_record.password_confirmation'].first)
          .to eq "doesn't match Password"
      end
    end
  end
end
