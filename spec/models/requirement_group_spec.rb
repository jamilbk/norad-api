# frozen_string_literal: true

# == Schema Information
#
# Table name: requirement_groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'rails_helper'

RSpec.describe RequirementGroup, type: :model do
  context 'when validating' do
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:description) }
  end

  context 'when maintaining associations' do
    it { should have_many(:requirements) }
    it { should have_many(:enforcements) }
    it { should have_many(:organizations).through(:enforcements) }
    it { should have_many(:security_containers).through(:requirements) }
  end
end
