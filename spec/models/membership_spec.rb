# frozen_string_literal: true

# == Schema Information
#
# Table name: memberships
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  organization_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_memberships_on_organization_id              (organization_id)
#  index_memberships_on_user_id                      (user_id)
#  index_memberships_on_user_id_and_organization_id  (user_id,organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_64267aab58  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_99326fb65d  (user_id => users.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe Membership, type: :model do
  context 'when maintaining associations' do
    it { should belong_to :user }
    it { should belong_to :organization }
  end

  it 'validates user uniqueness' do
    expect(create(:membership)).to validate_uniqueness_of(:user).scoped_to(:organization_id)
  end

  context 'when creating memberships' do
    before :each do
      @user = create :user
      @organization = create :organization
    end

    it 'an admin membership is created' do
      membership = Membership.create_admin!(@user, @organization)
      expect(membership.user.roles[-1].name).to eq('organization_admin')
      expect(membership.user.roles[-1].resource_type).to eq('Organization')
      expect(membership.user.roles[-1].resource_id).to eq(@organization.id)
    end

    it 'an reader membership is created' do
      membership = Membership.create_reader!(@user, @organization)
      expect(membership.user.roles[-1].name).to eq('organization_reader')
      expect(membership.user.roles[-1].resource_type).to eq('Organization')
      expect(membership.user.roles[-1].resource_id).to eq(@organization.id)
    end

    it 'each membership can have only one role' do
      Membership.create_admin!(@user, @organization)
      membership = Membership.create_reader!(@user, @organization)
      expect(membership.id).to be(nil)
    end
  end

  context 'when detroying memberships' do
    before :each do
      user = create :user
      org1 = create :organization
      org2 = create :organization
      @admin_membership = Membership.create_admin!(user, org1)
      @reader_membership = Membership.create_reader!(user, org2)
    end

    it 'destroy only the admin membership' do
      expect { @admin_membership.destroy! }.to change(Membership, :count).by(-1)
    end

    it 'destroy only the reader membership' do
      expect { @reader_membership.destroy! }.to change(Membership, :count).by(-1)
    end
  end
end
