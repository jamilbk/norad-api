# frozen_string_literal: true

# == Schema Information
#
# Table name: machines
#
#  id                 :integer          not null, primary key
#  organization_id    :integer
#  ip                 :inet
#  fqdn               :string
#  description        :text
#  machine_status     :integer          default("pending"), not null
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  name               :string           not null
#  use_fqdn_as_target :boolean          default(FALSE), not null
#
# Indexes
#
#  index_machines_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_machines_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_bd87ec17a7  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'models/shared_examples/organization_error_watcher'
require 'models/shared_examples/ignore_scope'
require 'models/shared_examples/protected_object'

RSpec.describe Machine, type: :model, with_docker_doubled: true do
  context 'when retrieving machine' do
    let(:machine) { create(:machine) }

    it 'should retrieve machine by id' do
      id = machine.id
      expect(Machine.find(id)).to eq machine
    end

    it 'should raise error if id does not exist' do
      non_existant_id = machine.id + 1
      expect { Machine.find(non_existant_id) }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'should retrieve machine by ip address' do
      ip = machine.ip
      expect(Machine.find(ip)).to eq machine
    end

    it 'should raise error if ip does not exist' do
      non_existant_ip = machine.ip.to_s.split('.').map { |x| (x.to_i + 1) % 255 }.join('.')
      expect { Machine.find(non_existant_ip) }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context 'when validating' do
    # Since we enforce some not null constraints, let's build a valid machine for the Shoulda
    # matchers to use.
    context 'fqdn validation' do
      subject { build(:machine, ip: '') }

      it { should validate_uniqueness_of(:name).scoped_to(:organization_id) }
      it { should validate_length_of(:name).is_at_most(255) }

      describe 'invalid IP addresses and fqdns' do
        it 'should be valid with FQDN www.cisco.com' do
          subject.fqdn = 'www.cisco.com'
          expect(subject).to be_valid
        end

        it 'should be valid with FQDN p309srv04.cisco.us' do
          subject.fqdn = 'p309srv04.cisco.us'
          expect(subject).to be_valid
        end

        it 'should not be valid with a FQDN with underscores' do
          subject.fqdn = 'p309srv04.cis_co.us'
          expect(subject).to_not be_valid
        end

        it 'should be valid with a FQDN with leading underscores' do
          subject.fqdn = '_p309srv04._cisco.us'
          expect(subject).to be_valid
        end

        it 'should be valid with a FQDN with an alphanumeric TLD' do
          subject.fqdn = 'www.cisco.com1'
          expect(subject).to be_valid
        end

        it 'should not be valid with a FQDN with a fully-numeric TLD' do
          subject.fqdn = 'www.cisco.28'
          expect(subject).to_not be_valid
        end

        it 'should be valid with a punycode-formatted FQDN' do
          subject.fqdn = 'xn--c1yn36f.com'
          expect(subject).to be_valid
        end

        it 'should not be valid with a FQDN label over 63 characters' do
          subject.fqdn = 'a' * 64 + '.com'
          expect(subject).to_not be_valid
        end

        it 'should be valid with a FQDN label 63 characters long' do
          subject.fqdn = 'a' * 63 + '.com'
          expect(subject).to be_valid
        end

        it 'should not be valid with a numberic FQDN' do
          subject.fqdn = '172.16.128'
          expect(subject).to_not be_valid
        end

        it 'should not be valid with fqdn localhost' do
          subject.fqdn = 'localhost'
          expect(subject).to_not be_valid
          expect(subject.errors[:fqdn]).to include('cannot be localhost')
        end
      end

      context 'ip address validation' do
        it 'should not be valid with IP address 127.0.0.1' do
          machine = build :machine, ip: '127.0.0.1'
          expect(machine).to_not be_valid
          expect(machine.errors[:ip]).to include('cannot be 127.0.0.1')
        end
      end
    end
  end

  context 'when validating with custom methods' do
    describe 'IP range provided' do
      let(:machine) { build :machine, ip: '10.0.0.0/8' }
      it 'should not be valid ' do
        expect(machine).to_not be_valid
        expect(machine.errors[:ip]).to include('address must be a valid, single IPv4 address')
      end
    end

    describe 'invalid IP address provided' do
      shared_examples 'it has an invalid ip' do
        it 'adds a validation error message to ip' do
          expect(machine).not_to be_valid
          expect(machine.ip).to be(nil)
          expect(machine.errors[:ip]).to include('address must be a valid, single IPv4 address')
        end
      end

      context 'invalid octect' do
        let(:machine) { build :machine, fqdn: nil, ip: '256.256.256.256' }
        it_behaves_like 'it has an invalid ip'
      end

      context 'invalid character' do
        let(:machine) { build :machine, fqdn: nil, ip: '192.168.b.2' }
        it_behaves_like 'it has an invalid ip'
      end

      context 'leading whitespace' do
        let(:machine) { build :machine, ip: ' 192.168.1.1' }
        it 'should strip whitespace' do
          expect(machine.ip).to eq '192.168.1.1'
          expect(machine).to be_valid
        end
      end

      context 'trailing whitespace' do
        let(:machine) { build :machine, ip: '192.168.1.1 ' }
        it 'should strip whitespace' do
          expect(machine.ip).to eq '192.168.1.1'
          expect(machine).to be_valid
        end
      end
    end

    describe 'properly formatted, single IP address' do
      let(:machine) { build :machine, ip: '192.168.1.1' }
      it 'should be valid' do
        expect(machine).to be_valid
      end
    end
  end

  context 'when maintaining associations' do
    it { should have_one(:ssh_key_pair).through(:ssh_key_pair_assignment) }
    it { should have_one(:ssh_key_pair_assignment) }
    it { should have_many(:service_discoveries) }
    it { should have_many(:result_ignore_rules).dependent(:destroy) }
  end

  context 'when executing callbacks' do
    before(:each) do
      allow_any_instance_of(SshConnectivityCheck).to receive(:eligible).and_return(true)
    end

    context 'before valiation' do
      it 'generates a UUID for a name if none is provided' do
        machine = Machine.new(name: nil)
        expect(machine.name).to be(nil)
        # Run the before_valiation callbacks
        machine.valid?
        expect(machine.name).to_not be(nil)
      end
    end

    context 'after saving' do
      it 'checks for Machine Connectivity Errors after a machine is created' do
        machine = build :machine
        expect(UnreachableMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToPingMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToSshToMachineError).to receive(:check).with(machine.organization, subject: machine)
        machine.save!
      end

      it 'checks for Machine Connectivity Errors after ip,fqdn is updated' do
        machine = create :machine
        expect(UnreachableMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToPingMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToSshToMachineError).to receive(:check).with(machine.organization, subject: machine)
        machine.ip = '172.28.18.1'
        machine.save!
      end

      it 'does not check for Machine Connectivity Errors unless ip,fqdn is updated' do
        machine = create :machine
        expect(UnreachableMachineError).not_to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToPingMachineError).not_to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToSshToMachineError).not_to receive(:check).with(machine.organization, subject: machine)
        machine.name = 'new name'
        machine.save!
      end
    end

    context 'after destroying' do
      let(:machine) { create :machine }

      it 'checks for the necessity of Machine Connectivity Errors after a machine is destroyed' do
        expect(UnreachableMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToPingMachineError).to receive(:check).with(machine.organization, subject: machine)
        expect(UnableToSshToMachineError).to receive(:check).with(machine.organization, subject: machine)
        machine.destroy
      end

      it 'destroys associated SSH Organization Errors after a machine is destroyed' do
        error1 = create :key_pair_assignment_without_ssh_service_error
        machine = error1.machine
        create :ssh_service_without_key_pair_assignment_error, errable: machine
        machine.destroy
        expect(SshServiceWithoutKeyPairAssignmentError.count).to eq 0
        expect(KeyPairAssignmentWithoutSshServiceError.count).to eq 0
      end
    end
  end

  context 'public class methods' do
    def create_assessment(machine, command, type, results)
      assessment = create type, machine: machine, docker_command: command
      results.each do |r|
        create :result, nid: 'qualys:qid1234', status: r, assessment: assessment
      end
      assessment
    end

    it 'returns a count of each result state for regardless of assessment type' do
      command = create :docker_command
      machine = command.machine
      create_assessment machine, command, :white_box_assessment, %i[pass fail]
      create_assessment machine, command, :white_box_assessment, %i[info error]
      create_assessment machine, command, :black_box_assessment, %i[pass fail]
      create_assessment machine, command, :black_box_assessment, %i[info error]
      h = machine.latest_assessment_stats
      expect(h[:passing]).to eq 2
      expect(h[:failing]).to eq 2
      expect(h[:erroring]).to eq 2
      expect(h[:informing]).to eq 2
    end

    context 'assessment statuses' do
      let(:command) { create :docker_command }
      let(:machine) { command.machine }

      it 'marks machine as erroring in presence of error status' do
        create_assessment machine, command, :white_box_assessment, %i[pass fail warn]
        create_assessment machine, command, :white_box_assessment, %i[info error]
        create_assessment machine, command, :black_box_assessment, %i[pass fail warn]
        create_assessment machine, command, :black_box_assessment, %i[info error]
        expect(machine.assessment_status).to eq :erroring
      end

      it 'marks machine as failing in presence of fail status' do
        create_assessment machine, command, :white_box_assessment, %i[pass fail warn]
        create_assessment machine, command, :white_box_assessment, %i[info pass]
        create_assessment machine, command, :black_box_assessment, %i[pass fail warn]
        create_assessment machine, command, :black_box_assessment, %i[info pass]
        expect(machine.assessment_status).to eq :failing
      end

      it 'marks machine as warning in presence of warn status' do
        create_assessment machine, command, :white_box_assessment, %i[pass warn]
        create_assessment machine, command, :white_box_assessment, %i[info pass]
        create_assessment machine, command, :black_box_assessment, %i[pass warn]
        create_assessment machine, command, :black_box_assessment, %i[info pass]
        expect(machine.assessment_status).to eq :warning
      end

      it 'marks machine as passing in presence of passing (and optionally, info) status' do
        create_assessment machine, command, :white_box_assessment, %i[pass pass]
        create_assessment machine, command, :white_box_assessment, %i[info pass]
        create_assessment machine, command, :black_box_assessment, %i[pass pass]
        create_assessment machine, command, :black_box_assessment, %i[info pass]
        expect(machine.assessment_status).to eq :passing
      end

      it 'marks machine as informing in presence of only info status' do
        create_assessment machine, command, :white_box_assessment, %i[info info]
        create_assessment machine, command, :black_box_assessment, %i[info info]
        expect(machine.assessment_status).to eq :informing
      end

      it 'marks machine as pending when lacking results' do
        expect(machine.assessment_status).to eq :pending
      end
    end
  end

  context 'when receiving instance methods' do
    context '#target_address' do
      let(:machine) { build(:machine) }

      context 'when use_fqdn_as_target is false' do
        before(:each) { machine.use_fqdn_as_target = false }

        context 'when ip and fqdn are set' do
          before(:each) do
            machine.ip = '172.28.18.1'
            machine.fqdn = 'factory.server.local'
          end

          it 'should return ip as target_address' do
            expect(machine.target_address).to eq(machine.ip_to_s)
          end
        end

        context 'when ip is not set but fqdn is' do
          before(:each) do
            machine.ip = nil
            machine.fqdn = 'factory.server.local'
          end

          it 'should return fqdn as target_address' do
            expect(machine.target_address).to eq(machine.fqdn)
          end
        end
      end

      context 'when use_fqdn_as_target is true' do
        before(:each) { machine.use_fqdn_as_target = true }

        context 'when ip is set but fqdn is not' do
          before(:each) do
            machine.ip = '172.28.18.1'
            machine.fqdn = nil
          end

          it 'should trigger a validation error' do
            expect(machine).to_not be_valid
            expect(machine.errors[:use_fqdn_as_target]).to include('cannot be true if fqdn is not present')
          end
        end

        context 'when ip is not set but fqdn is' do
          before(:each) do
            machine.ip = nil
            machine.fqdn = 'factory.server.local'
          end

          it 'should return fqdn as target_address' do
            expect(machine.target_address).to eq(machine.fqdn)
          end
        end
      end
    end

    context '#ssh_key_values' do
      before :each do
        org = create :organization
        @config = org.configuration
        @machine = create :machine, organization: org
      end

      it 'returns the values of an SshConfig object if Organization is configured to use them' do
        @config.use_relay_ssh_key = false
        @config.save!
        ssh_key = create :ssh_key_pair, organization: @machine.organization
        @machine.ssh_key_pair = ssh_key

        values = @machine.ssh_key_values

        expect(values[:ssh_user]).to eq(ssh_key.username)
        expect(values[:ssh_key]).to eq(ssh_key.key)
      end

      it 'returns a placeholder value if Organization is configured to use Relay values' do
        @config.use_relay_ssh_key = true
        @config.save!
        ssh_key = create :ssh_key_pair, organization: @machine.organization
        @machine.ssh_key_pair = ssh_key

        values = @machine.ssh_key_values

        expect(values[:ssh_user]).to eq('%{ssh_user}')
        expect(values[:ssh_key]).to eq('%{ssh_key}')
      end
    end

    it 'returns the latest effective docker command via #latest_command' do
      machine = create :machine
      create :docker_command, commandable: machine
      org_command = create :docker_command, commandable: machine.organization

      expect(machine.latest_command).to eq(org_command)
    end

    it 'returns the object it inherits its RBAC from via #rbac_parent' do
      machine = build_stubbed :machine
      expect(machine.rbac_parent).to eq(machine.organization)
    end
  end

  context 'deleting a machine' do
    let(:command) { create :docker_command }
    let(:machine) { command.machine }

    it 'deletes machine and docker commands' do
      Machine.find(command.machine_id).destroy!

      expect { Machine.find(command.machine_id) }.to raise_error(ActiveRecord::RecordNotFound)
      expect { command.reload }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end

  context 'when watching for organization errors' do
    it_behaves_like 'an Organization Error Watcher'
  end

  it_behaves_like 'an Ignore Scope'
  it_behaves_like 'a Protected Object'
end
