# frozen_string_literal: true

# == Schema Information
#
# Table name: machine_connectivity_checks
#
#  id                           :integer          not null, primary key
#  machine_id                   :integer          not null
#  security_container_secret_id :integer
#  finished_at                  :datetime
#  created_at                   :datetime         not null
#  updated_at                   :datetime         not null
#  status                       :integer
#  type                         :string           not null
#  status_reason                :text
#
# Indexes
#
#  index_machine_connectivity_checks_on_finished_at          (finished_at)
#  index_machine_connectivity_checks_on_machine_id_and_type  (machine_id,type)
#  index_machine_connectivity_checks_on_s_c_s_id             (security_container_secret_id) UNIQUE
#  index_machine_connectivity_checks_on_status               (status)
#  index_machine_connectivity_checks_on_updated_at           (updated_at)
#
# Foreign Keys
#
#  fk_rails_74009af901  (machine_id => machines.id) ON DELETE => cascade
#  fk_rails_7a47e669da  (security_container_secret_id => security_container_secrets.id) ON DELETE => nullify
#

require 'rails_helper'

RSpec.describe SshConnectivityCheck, type: :model do
  context 'when validating' do
    describe '#eligible' do
      let(:machine) { create(:machine) }
      let(:check) { build(:ssh_connectivity_check, machine: machine) }

      it 'fails to save when key pair is missing' do
        check.valid?
        expect(check.errors['machine']).to include 'must be able to use a valid SSH key pair'
      end

      it 'fails to save when ssh service is not registered' do
        machine.ssh_key_pair = create(:ssh_key_pair)
        check.valid?
        expect(check.errors['machine']).to include 'must be able to use a valid SSH key pair'
      end

      it 'saves if key pair is present and ssh service is registered' do
        machine.ssh_key_pair = create(:ssh_key_pair)
        machine.ssh_services << create(:ssh_service)
        expect(check.valid?).to eq true
      end
    end
  end
end
