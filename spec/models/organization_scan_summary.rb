# frozen_string_literal: true

require 'rails_helper'

RSpec.describe OrganizationScanSummary, type: :model do
  let(:relation) { Machine.unscoped }
  let(:instance) { described_class.new(machines: relation) }

  describe '#query' do
    it 'returns a new ScanSummaryQuery' do
      expect(instance.query).to be_a ScanSummaryQuery
    end

    it 'sets the relation for ScanSummaryQuery' do
      expect(instance.query.relation).to eq relation
    end
  end
end
