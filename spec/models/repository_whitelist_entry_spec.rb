# frozen_string_literal: true

# == Schema Information
#
# Table name: repository_whitelist_entries
#
#  id                          :integer          not null, primary key
#  organization_id             :integer          not null
#  security_test_repository_id :integer          not null
#  created_at                  :datetime         not null
#  updated_at                  :datetime         not null
#
# Indexes
#
#  index_r_w_entries_on_o_id_and_s_t_r_id  (organization_id,security_test_repository_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_6d4fcfc98d  (security_test_repository_id => security_test_repositories.id) ON DELETE => cascade
#  fk_rails_daf54902a6  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe RepositoryWhitelistEntry, type: :model do
  context 'when maintaining associations' do
    it { should belong_to :organization }
    it { should belong_to :security_test_repository }
  end

  context 'validations' do
    let(:public_repository) { create(:public_repository) }
    let(:official_repository) { create(:official_repository) }
    let(:entry) { described_class.new }
    let(:organization) { create(:organization) }

    it 'allows public repositories to enter the whitelist' do
      entry.security_test_repository = public_repository
      entry.organization = organization
      expect(entry.valid?).to be_truthy
    end

    it 'prevents adding official repositories' do
      entry.security_test_repository = official_repository
      entry.organization = organization
      expect(entry.valid?).to be_falsey
      expect(entry.errors[:base]).to include "Official Norad Repository can't be modified, removed, or whitelisted"
    end

    it 'prevents adding duplicate entries' do
      entry.security_test_repository = public_repository
      entry.organization = organization
      expect(entry.save!).to be_truthy
      new_entry = described_class.new(security_test_repository: public_repository, organization: organization)
      expect(new_entry.valid?).to be_falsey
    end
  end
end
