# frozen_string_literal: true

# == Schema Information
#
# Table name: organization_errors
#
#  id              :integer          not null, primary key
#  organization_id :integer
#  type            :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  errable_type    :string
#  errable_id      :integer
#
# Indexes
#
#  index_organization_errors_on_o_id_and_type_and_e_type_and_e_id  (organization_id,errable_id,errable_type,type) UNIQUE
#
# Foreign Keys
#
#  fk_rails_823ceaeacb  (organization_id => organizations.id) ON DELETE => cascade
#

require 'rails_helper'
require 'aasm/rspec'
require 'models/shared_examples/organization_errors'

RSpec.describe RelayOfflineError, type: :model do
  it_behaves_like 'an Organization Error class'

  before :each do
    allow_any_instance_of(DockerRelay).to receive(:check_for_organization_errors)
  end

  describe '::check method' do
    before :each do
      @organization = create :organization
      @relay = create :docker_relay, organization: @organization
      @relay.verified = true
      @relay.save
    end

    context 'error creation' do
      it 'sets an error when primary relay goes offline' do
        @organization.primary_relay.go_offline!
        described_class.check(@organization)
        expect(@organization.organization_errors).not_to be_empty
      end

      it 'sets an error when new relay becomes primary and is offline' do
        relay2 = create :docker_relay, organization: @organization
        relay2.verified = true
        relay2.save

        @relay.go_offline!
        described_class.check(@organization)
        expect(@relay.organization.organization_errors).to be_empty

        @organization.primary_relay.destroy
        described_class.check(@organization)
        expect(@relay.organization.organization_errors.reload).not_to be_empty
      end
    end

    context 'error removal' do
      it 'removes an error when primary relay comes online' do
        @organization.primary_relay.go_offline!
        described_class.check(@organization)
        expect(@organization.organization_errors).not_to be_empty

        @organization.primary_relay.go_online!
        described_class.check(@organization)
        expect(@organization.organization_errors.reload).to be_empty
      end

      it 'removes an error when primary relay becomes unverified and no others exist' do
        @organization.primary_relay.go_offline!
        described_class.check(@organization)
        expect(@organization.organization_errors).not_to be_empty

        @relay.verified = false
        @relay.save
        described_class.check(@organization)
        expect(@organization.organization_errors.reload).to be_empty
      end

      it 'removes an error when new primary relay is online' do
        relay2 = create :docker_relay, organization: @organization
        relay2.verified = true
        relay2.save
        relay2.go_offline!

        described_class.check(@organization)
        expect(@organization.organization_errors).not_to be_empty

        @organization.primary_relay.destroy
        described_class.check(@organization)
        expect(@organization.organization_errors.reload).to be_empty
      end

      it 'removes an error when only relay is deleted from org' do
        @organization.primary_relay.go_offline!
        described_class.check(@organization)
        expect(@organization.organization_errors).not_to be_empty

        @organization.primary_relay.destroy
        described_class.check(@organization)
        expect(@organization.organization_errors.reload).to be_empty
      end
    end
  end
end
