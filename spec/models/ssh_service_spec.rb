# frozen_string_literal: true

# == Schema Information
#
# Table name: services
#
#  id                  :integer          not null, primary key
#  name                :string           not null
#  description         :text
#  port                :integer          not null
#  port_type           :integer          default("tcp"), not null
#  encryption_type     :integer          default("cleartext"), not null
#  machine_id          :integer
#  type                :string           not null
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  allow_brute_force   :boolean          default(FALSE), not null
#  application_type_id :integer
#  discovered          :boolean          default(FALSE), not null
#
# Indexes
#
#  index_services_on_application_type_id  (application_type_id)
#  index_services_on_machine_id           (machine_id)
#  index_services_on_machine_id_and_port  (machine_id,port) UNIQUE
#  index_services_on_type                 (type)
#
# Foreign Keys
#
#  fk_rails_6a8ba918c1  (application_type_id => application_types.id)
#  fk_rails_b32a34656d  (machine_id => machines.id) ON DELETE => cascade
#

require 'rails_helper'

RSpec.describe SshService, type: :model do
  context 'when executing callbacks' do
    context 'before validating' do
      before :each do
        @ssh = build :ssh_service
      end

      it 'ensures the port type is TCP' do
        @ssh.port_type = :udp
        @ssh.valid?
        expect(@ssh.port_type).to eq('tcp')
      end

      it 'ensures the encryption type is SSH' do
        @ssh.encryption_type = :ssl
        @ssh.valid?
        expect(@ssh.encryption_type).to eq('ssh')
      end

      it 'sets the port to 22 if none is set' do
        @ssh.port = nil
        @ssh.valid?
        expect(@ssh.port).to eq(22)
      end
    end

    context 'before saving' do
      it 'is always associated with the ssh service type' do
        ssh = build :ssh_service, port: 1234
        expect(ssh.application_type).to be(nil)
        ssh.save!
        cst = ApplicationType.find_by(name: 'ssh', port: 22, transport_protocol: 'tcp')
        expect(ssh.application_type).to eq(cst)
      end
    end
  end

  context 'checks for SSH Organization Errors' do
    it 'after creating' do
      ssh_service = build :ssh_service
      org = ssh_service.organization
      machine = ssh_service.machine
      expect(KeyPairAssignmentWithoutSshServiceError).to receive(:check).with(org, subject: machine)
      expect(SshServiceWithoutKeyPairAssignmentError).to receive(:check).with(org, subject: machine)
      ssh_service.save!
    end

    context do
      let!(:ssh_service) { create :ssh_service }

      it 'after destroying' do
        org = ssh_service.organization
        machine = ssh_service.machine
        expect(KeyPairAssignmentWithoutSshServiceError).to receive(:check).with(org, subject: machine)
        expect(SshServiceWithoutKeyPairAssignmentError).to receive(:check).with(org, subject: machine)
        ssh_service.destroy
      end
    end
  end

  context 'skips check for SSH Organization Errors' do
    let!(:ssh_service) { create :ssh_service }

    it 'after updating' do
      ssh_service.update!(name: 'ssh_service')
      expect(KeyPairAssignmentWithoutSshServiceError).not_to receive(:check)
      expect(SshServiceWithoutKeyPairAssignmentError).not_to receive(:check)
    end
  end

  context 'checks for Machine Connectivity Check Errors' do
    context 'after creating' do
      let(:assignment) { create :ssh_key_pair_assignment }
      let!(:ssh_service) { build :ssh_service, machine: assignment.machine.reload }

      it 'checks UnableToSshToMachineError' do
        expect(UnableToSshToMachineError)
          .to receive(:check).with(ssh_service.organization, subject: ssh_service.machine)
        ssh_service.save!
      end
    end

    context 'after updating' do
      let!(:ssh_service) { create :ssh_service, port: 22 }
      let(:machine) { ssh_service.machine }
      let(:org) { ssh_service.organization }

      it 'checks UnableToSshToMachineError for port attribute' do
        ssh_service.port = 2222
        expect(UnableToSshToMachineError).to receive(:check).with(org, subject: machine)
        ssh_service.save!
      end

      it 'does not check UnableToSshToMachineError for other attributes' do
        ssh_service.update!(name: 'new name')
        ssh_service.update!(description: 'new desc')
        expect(UnableToSshToMachineError).not_to receive(:check)
      end
    end

    context 'before destroying' do
      it 'removes UnableToSshToMachineErrors' do
        ssh_service = create :ssh_service
        machine = ssh_service.machine
        create :ssh_key_pair_assignment, machine: machine
        create :unable_to_ssh_to_machine_error, errable: machine.reload, organization: machine.organization
        expect(SshServiceWithoutKeyPairAssignmentError.count).to eq 0
        expect { ssh_service.destroy }.to change(UnableToSshToMachineError, :count).by(-1)
      end
    end
  end
end
