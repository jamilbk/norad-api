# frozen_string_literal: true

require 'rails_helper'
require 'query_objects/shared_examples/query_object'

RSpec.describe MachineCommandsQuery do
  it_behaves_like 'a Query Object' do
    let(:subject) { described_class.new(double) }
  end

  it 'returns Docker Commands for a set of machines via #relation' do
    machine = create :machine
    command1 = create :docker_command, commandable: machine
    command2 = create :docker_command, commandable: machine
    create :docker_command # create a command not tied to the machine
    query = MachineCommandsQuery.new(Machine.where(id: machine))

    expect(query.relation).to match_array(DockerCommand.where(id: [command1, command2]))
  end

  it 'returns a unique docker command for each machine via #unique_commands' do
    machine1 = create :machine
    machine2 = create :machine, organization: machine1.organization
    2.times do
      create :docker_command, commandable: machine1
      create :docker_command, commandable: machine2
    end
    query = MachineCommandsQuery.new(Machine.where(id: [machine1, machine2]))

    expect(query.unique_commands.map(&:machine_id)).to match_array([machine1.id, machine2.id])
  end

  describe '#unique_commands_newer_than' do
    let(:organization) { create :organization }
    let(:machine1) { create(:machine, organization: organization) }
    let(:machine2) { create(:machine, organization: organization) }

    before(:each) do
      # Create some noise in the database
      create :docker_command, machine: machine1
      create :docker_command, commandable: organization
      create :docker_command, commandable: machine2
    end

    it 'returns unique commands newer than a cutoff' do
      # Create some more noise in the DB
      create :docker_command, machine: machine1

      # Create the DC we care about
      org_command = create :docker_command, commandable: organization
      latest_for_machine2 = create :docker_command, commandable: machine2
      query = MachineCommandsQuery.new(Machine.where(id: organization.machines))

      new_commands = query.unique_commands_newer_than(org_command.created_at)
      expect(new_commands).to match_array(DockerCommand.where(id: latest_for_machine2))
    end

    it 'returns all unique commands if no cutoff is specified' do
      create :docker_command, machine: machine1
      latest_for_machine1 = create :docker_command, machine: machine1
      create :docker_command, commandable: organization
      latest_for_machine2 = create :docker_command, machine: machine2
      query = MachineCommandsQuery.new(Machine.where(id: organization.machines))

      new_commands = query.unique_commands_newer_than(nil)
      expect(new_commands).to match_array(DockerCommand.where(id: [latest_for_machine1, latest_for_machine2]))
    end
  end
end
