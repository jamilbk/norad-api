# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pair_assignments
#
#  id              :integer          not null, primary key
#  machine_id      :integer          not null
#  ssh_key_pair_id :integer          not null
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#
# Indexes
#
#  index_ssh_key_pair_assignments_on_machine_id       (machine_id) UNIQUE
#  index_ssh_key_pair_assignments_on_ssh_key_pair_id  (ssh_key_pair_id)
#
# Foreign Keys
#
#  fk_rails_05e7648bab  (ssh_key_pair_id => ssh_key_pairs.id) ON DELETE => cascade
#  fk_rails_7c3731b704  (machine_id => machines.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :ssh_key_pair_assignment do
    machine
    ssh_key_pair { build(:ssh_key_pair, organization: machine.organization) }
  end
end
