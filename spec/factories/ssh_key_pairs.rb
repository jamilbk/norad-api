# frozen_string_literal: true

# == Schema Information
#
# Table name: ssh_key_pairs
#
#  id                 :integer          not null, primary key
#  name               :string           not null
#  description        :text
#  username_encrypted :string
#  key_encrypted      :string
#  key_signature      :string           not null
#  organization_id    :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#
# Indexes
#
#  index_ssh_key_pairs_on_name_and_organization_id  (name,organization_id) UNIQUE
#  index_ssh_key_pairs_on_organization_id           (organization_id)
#
# Foreign Keys
#
#  fk_rails_3811d4539d  (organization_id => organizations.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :ssh_key_pair do
    name { "Factory Key: #{SecureRandom.hex}" }
    description 'MyText'
    username 'ssh-user'
    key { Base64.strict_encode64(OpenSSL::PKey::RSA.new(2048).to_pem) }
    organization
  end
end
