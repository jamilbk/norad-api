# frozen_string_literal: true

# == Schema Information
#
# Table name: enforcements
#
#  id                   :integer          not null, primary key
#  requirement_group_id :integer          not null
#  organization_id      :integer          not null
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
# Indexes
#
#  index_enforcements_on_organization_id                           (organization_id)
#  index_enforcements_on_requirement_group_id                      (requirement_group_id)
#  index_enforcements_on_requirement_group_id_and_organization_id  (requirement_group_id,organization_id) UNIQUE
#
# Foreign Keys
#
#  fk_rails_33fbeb613a  (organization_id => organizations.id) ON DELETE => cascade
#  fk_rails_50f7901392  (requirement_group_id => requirement_groups.id) ON DELETE => cascade
#

FactoryBot.define do
  factory :enforcement do
    requirement_group
    organization
  end
end
