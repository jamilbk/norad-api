# frozen_string_literal: true

# == Schema Information
#
# Table name: requirement_groups
#
#  id          :integer          not null, primary key
#  name        :string
#  description :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

FactoryBot.define do
  factory :requirement_group do
    name 'MyString'
    description 'MyString'
  end
end
