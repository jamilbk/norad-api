# frozen_string_literal: true

require 'rails_helper'

describe MachineScanScheduleAuthorizer, type: :authorizer, with_resque_doubled: true do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
    @unprivileged_user_other = create :user
    @admin_user_other = create :user
    @org1 = create :organization
    @org2 = create :organization
    @admin_user.add_role :organization_admin, @org1
    @admin_user_other.add_role :organization_admin, @org2
    @unprivileged_user.add_role :organization_reader, @org1
    @unprivileged_user_other.add_role :organization_reader, @org2
    @options = { in: @org1 }
  end

  context 'at the class level' do
    it 'lets admin users create' do
      expect(MachineScanSchedule.authorizer).to be_creatable_by(@admin_user, @options)
      expect(MachineScanSchedule.authorizer).to_not be_creatable_by(@admin_user_other, @options)
      expect(MachineScanSchedule.authorizer).to_not be_creatable_by(@unprivileged_user, @options)
      expect(MachineScanSchedule.authorizer).to_not be_creatable_by(@unprivileged_user_other, @options)
    end

    it 'lets admin and reader users read' do
      expect(MachineScanSchedule.authorizer).to be_readable_by(@admin_user, @options)
      expect(MachineScanSchedule.authorizer).to_not be_readable_by(@admin_user_other, @options)
      expect(MachineScanSchedule.authorizer).to be_readable_by(@unprivileged_user, @options)
      expect(MachineScanSchedule.authorizer).to_not be_readable_by(@unprivileged_user_other, @options)
    end
  end

  context 'at the instance level' do
    before :each do
      machine = create :machine, organization: @org1
      @schedule = create :machine_scan_schedule, machine: machine
    end

    it 'requires admin privileges to delete' do
      expect(@schedule.authorizer).to be_deletable_by(@admin_user)
      expect(@schedule.authorizer).to_not be_deletable_by(@admin_user_other)
      expect(@schedule.authorizer).to_not be_deletable_by(@unprivileged_user)
      expect(@schedule.authorizer).to_not be_deletable_by(@unprivileged_user_other)
    end

    it 'requires admin privileges to update' do
      expect(@schedule.authorizer).to be_updatable_by(@admin_user)
      expect(@schedule.authorizer).to_not be_updatable_by(@admin_user_other)
      expect(@schedule.authorizer).to_not be_updatable_by(@unprivileged_user)
      expect(@schedule.authorizer).to_not be_updatable_by(@unprivileged_user_other)
    end

    it 'lets admin and reader users read' do
      expect(@schedule.authorizer).to be_readable_by(@admin_user)
      expect(@schedule.authorizer).to_not be_readable_by(@admin_user_other)
      expect(@schedule.authorizer).to be_readable_by(@unprivileged_user)
      expect(@schedule.authorizer).to_not be_readable_by(@unprivileged_user_other)
    end
  end
end
