# frozen_string_literal: true

require 'rails_helper'

describe MachineConnectivityCheckAuthorizer, type: :authorizer do
  before(:each) do
    @org = create :organization
    @admin = create :user
    @reader = create :user
    @other = create :user

    @admin.add_role :organization_admin, @org
    @reader.add_role :organization_reader, @org

    machine = create :machine, organization: @org
    @check = create :machine_connectivity_check, machine: machine
  end

  context 'machine_connectivity_check action' do
    it 'try to read at the class level' do
      expect(MachineConnectivityCheck.authorizer).to be_readable_by @admin, in: @org
      expect(MachineConnectivityCheck.authorizer).to be_readable_by @reader, in: @org
      expect(MachineConnectivityCheck.authorizer).not_to be_readable_by @other, in: @org
    end

    it 'try to read at the instance level' do
      expect(@check.authorizer).to be_readable_by @admin
      expect(@check.authorizer).to be_readable_by @reader
      expect(@check.authorizer).not_to be_readable_by @other
    end

    it 'try to create' do
      expect(MachineConnectivityCheck.authorizer).to be_creatable_by @admin, in: @org
      expect(MachineConnectivityCheck.authorizer).not_to be_creatable_by @reader, in: @org
      expect(MachineConnectivityCheck.authorizer).not_to be_creatable_by @other, in: @org
    end
  end
end
