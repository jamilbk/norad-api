# frozen_string_literal: true

require 'rails_helper'

describe EnforcementAuthorizer, type: :authorizer do
  before :each do
    @admin_user = create :user
    @unprivileged_user = create :user
    @unprivileged_user_other = create :user
    @admin_user_other = create :user
  end

  before :each do
    @org1 = create :organization
    @org2 = create :organization
    @admin_user.add_role :organization_admin, @org1
    @admin_user_other.add_role :organization_admin, @org2
    @unprivileged_user.add_role :organization_reader, @org1
    @unprivileged_user_other.add_role :organization_reader, @org2
    @enforcement = create :enforcement, organization: @org1
    @options = { in: @org1 }
  end

  it 'allows org admins to create' do
    expect(Enforcement.authorizer).to be_creatable_by(@admin_user, @options)
    expect(Enforcement.authorizer).to_not be_creatable_by(@admin_user_other, @options)
    expect(Enforcement.authorizer).to_not be_creatable_by(@unprivileged_user, @options)
    expect(Enforcement.authorizer).to_not be_creatable_by(@unprivileged_user_other, @options)
  end

  it 'allows org admins to delete' do
    expect(@enforcement.authorizer).to be_deletable_by(@admin_user)
    expect(@enforcement.authorizer).to_not be_deletable_by(@admin_user_other)
    expect(@enforcement.authorizer).to_not be_deletable_by(@unprivileged_user)
    expect(@enforcement.authorizer).to_not be_deletable_by(@unprivileged_user_other)
  end

  it 'allows org admins and readers to read at the class level' do
    expect(Enforcement.authorizer).to be_readable_by(@admin_user, @options)
    expect(Enforcement.authorizer).to_not be_readable_by(@admin_user_other, @options)
    expect(Enforcement.authorizer).to be_readable_by(@unprivileged_user, @options)
    expect(Enforcement.authorizer).to_not be_readable_by(@unprivileged_user_other, @options)
  end
end
