# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe TimeoutStaleConnectivityChecksJob, type: :job, with_resque_doubled: true do
  let(:machine) { create :machine }

  before :each do
    allow(@resque_module_double).to receive(:logger)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'looks for stale ConnectivityChecks to time out' do
    expect(MachineConnectivityCheck).to receive(:stale).and_call_original
    perform_enqueued_jobs { job }
  end

  it 'creates an associated error if the stale check was the latest' do
    older_stale_check = create :machine_connectivity_check, machine: machine, updated_at: 1.day.ago,
                                                            type: 'PingConnectivityCheck'
    finished_check = create :finished_machine_connectivity_check, machine: machine, type: 'PingConnectivityCheck'
    stale_check = create :stale_machine_connectivity_check, machine: machine, type: 'PingConnectivityCheck'

    relation = double
    allow(relation).to receive(:find_each)
      .and_yield(older_stale_check)
      .and_yield(stale_check)
    allow(relation).to receive(:count)

    expect(MachineConnectivityCheck).to receive(:stale).and_return(relation)
    expect(stale_check).to receive(:create_error).and_call_original
    expect(finished_check).not_to receive(:create_error)
    expect(older_stale_check).not_to receive(:create_error)
    expect do
      perform_enqueued_jobs { job }
    end.to change(stale_check.class::ORG_ERROR_KLASS.constantize, :count).by(1)
    expect(stale_check.status_reason).to eq MachineConnectivityCheck::TIMED_OUT_STATUS_REASON
    expect(older_stale_check.status_reason).to eq MachineConnectivityCheck::TIMED_OUT_STATUS_REASON
    expect(finished_check.status_reason).to eq nil
  end
end
