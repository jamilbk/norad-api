# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe SyncRelayVersionJob, type: :job, with_resque_doubled: true do
  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'calls RelayImageInfoFetcher#fetch_and_cache_latest_version' do
    expect(RelayImageInfoFetcher).to receive(:fetch_and_cache_latest_version).once
    perform_enqueued_jobs { job }
  end

  describe '.refresh_stale_records' do
    context 'when latest relay version is not available' do
      before :each do
        allow(RelayImageInfoFetcher).to(receive(:fetch_and_cache_latest_version)).and_return(nil)
      end

      context 'when Relays have versions reported' do
        let!(:outdated_docker_relay) do
          create(:docker_relay, last_reported_version: '0.0.1', outdated: false, state: :offline)
        end

        let!(:not_outdated_docker_relay) do
          create(:docker_relay, last_reported_version: '0.0.2', outdated: false, state: :offline)
        end

        let!(:unreported_docker_relay) { create(:docker_relay, outdated: false, state: :offline) }

        it 'should not update any records' do
          expect(DockerRelay).not_to receive(:update_all)

          perform_enqueued_jobs { job }

          expect(outdated_docker_relay.reload).to_not be_outdated
          expect(not_outdated_docker_relay.reload).to_not be_outdated
          expect(unreported_docker_relay.reload).to_not be_outdated
        end
      end
    end

    context 'when latest_relay_version is available' do
      before :each do
        allow(RelayImageInfoFetcher).to(receive(:fetch_and_cache_latest_version)).and_return('0.0.2')
      end

      context 'when Relays have versions reported' do
        let!(:outdated_docker_relay) do
          create(:docker_relay, last_reported_version: '0.0.1', outdated: false, state: :offline)
        end

        let!(:not_outdated_docker_relay) do
          create(:docker_relay, last_reported_version: '0.0.2', outdated: false, state: :offline)
        end

        it 'updates outdated records' do
          perform_enqueued_jobs { job }

          expect(outdated_docker_relay.reload).to be_outdated
          expect(not_outdated_docker_relay.reload).to_not be_outdated
        end
      end

      context 'when Relays have not reported their versions' do
        let!(:unreported_docker_relay) { create(:docker_relay, outdated: false, state: :offline) }

        it 'should not update Relays' do
          perform_enqueued_jobs { job }

          # outdated defaults to FALSE for new records
          expect(unreported_docker_relay.reload).not_to be_outdated
        end
      end
    end
  end
end
