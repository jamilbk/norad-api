# frozen_string_literal: true

require 'rails_helper'
require 'jobs/shared_examples/recurring_job.rb'

RSpec.describe SyncScanSchedulesJob, type: :job, with_resque_doubled: true do
  before :each do
    allow(@resque_module_double).to receive(:logger)
    allow(@resque_module_double).to receive(:schedule).and_return(
      machine_scan_schedule_1: 1,
      machine_scan_schedule_2: 2,
      organization_scan_schedule_1: 1,
      organization_scan_schedule_2: 2,
      foo: :bar
    )
    @validator_double = instance_double(ScanScheduleValidator)
    allow(@validator_double).to receive(:validate)
  end

  after :each do
    clear_enqueued_jobs
    clear_performed_jobs
  end

  subject(:job) { described_class.perform }

  it_behaves_like 'a Recurring Job'

  it 'calls the validator once for each schedule key' do
    expect(ScanScheduleValidator).to(
      receive(:new).with(MachineScanSchedule, /machine/).at_least(:twice).and_return(@validator_double)
    )
    expect(ScanScheduleValidator).to(
      receive(:new).with(OrganizationScanSchedule, /org/).at_least(:twice).and_return(@validator_double)
    )
    perform_enqueued_jobs { job }
  end
end

RSpec.describe ScanScheduleValidator, with_resque_doubled: true do
  describe 'instance methods' do
    before :each do
      ar_double = double('An AR Object')
      allow(ar_double).to receive(:find_by)
      @validator = described_class.new ar_double, ''
    end

    describe '#validate' do
      it 'removes the schedule in Resque if no corresponding ScanSchedule exists' do
        allow(@validator).to receive(:scan).and_return(nil)
        expect(@validator).to receive(:remove_schedule)
        @validator.validate
      end

      context 'queue and scan object are out of sync' do
        it 'updates the queue if the scan object is valid' do
          scan_double = double('A scan')
          allow(@validator).to receive(:scan).and_return(scan_double)
          allow(scan_double).to receive(:valid?).and_return(true)
          allow(scan_double).to receive(:cron_string).and_return('asdf')
          allow(@resque_module_double).to receive(:fetch_schedule).and_return('cron' => '1234')
          expect(scan_double).to receive(:update_scan_in_queue)
          @validator.validate
        end

        it 'removes the item from the queue if the scan object is invalid' do
          scan_double = double('A scan')
          allow(@validator).to receive(:scan).and_return(scan_double)
          allow(scan_double).to receive(:valid?).and_return(false)
          allow(scan_double).to receive(:cron_string).and_return('asdf')
          allow(@resque_module_double).to receive(:fetch_schedule).and_return('cron' => '1234')
          expect(@validator).to receive(:remove_schedule)
          @validator.validate
        end
      end
    end
  end
end
