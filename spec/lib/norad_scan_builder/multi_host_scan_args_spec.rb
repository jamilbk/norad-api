# frozen_string_literal: true

require 'rails_helper'
require './lib/norad_scan_builder'
require 'lib/norad_scan_builder/shared_examples/scan_args'

RSpec.describe NoradScanBuilder::MultiHostScanArgs, with_resque_doubled: true do
  it_should_behave_like 'a Scan Args class' do
    let(:options) { { machines: [build_stubbed(:machine)], organization: build_stubbed(:organization) } }
  end

  describe '#to_a' do
    let(:organization) { create(:organization) }
    let(:ip) { "192.168.1.#{rand(1..255)}" }
    let(:machine) { create(:machine, ip: ip, fqdn: nil) }
    let(:container) { create(:multi_host_test) }
    let(:scan_args) { described_class.new(container, machines: organization.machines, organization: organization) }

    before(:each) do
      5.times { organization.machines << build(:machine) }
    end

    it 'uses the machines for the targets' do
      expect(scan_args.to_a.first.split(',')).to match_array(organization.machines.map(&:target_address))
    end

    it 'uses a subset of the organization machines if passed' do
      machines = organization.machines.sample(3)
      scan_args = described_class.new(container, machines: machines, organization: organization)
      expect(scan_args.to_a.first.split(',')).to match_array(machines.map(&:target_address))
    end

    context 'for a configurable test' do
      let(:default_foo) { 'bar' }
      let(:prog_args) { '%{target} -x %{foo}' }
      let(:container) do
        create(:multi_host_test, configurable: true, default_config: { foo: default_foo }, prog_args: prog_args)
      end

      before(:each) do
        5.times { organization.machines << build(:machine) }
      end

      it 'uses the organization config even if a machine config exists' do
        machine = organization.machines.sample(1).first

        create(:security_container_config,
               configurable: organization,
               values: { 'foo' => 'baz' },
               security_container: container)
        create(:security_container_config,
               configurable: machine,
               values: { 'foo' => 'asdf' },
               security_container: container)

        expect(scan_args.to_a).to include('baz')
        expect(scan_args.to_a).to_not include(default_foo)
        expect(scan_args.to_a).to_not include('asdf')
      end

      it 'uses the default config if no organization config exists' do
        expect(scan_args.to_a).to include(default_foo)
      end
    end
  end
end
