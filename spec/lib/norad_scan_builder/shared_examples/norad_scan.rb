# frozen_string_literal: true

RSpec.shared_examples 'a Norad Scan' do
  let(:options) { {} }

  describe 'instantion' do
    it 'sends a message to an args class to build up the argument list' do
      allow(target_klass).to receive(:new)
      expect(args_klass).to receive(:new)
      described_class.new(double, double, double, options)
    end

    it 'sends a message to a target class to build up the targets' do
      allow(args_klass).to receive(:new).and_return([1]) # need a non-empty object that responds to to_a
      expect(target_klass).to receive(:new)
      described_class.new(double, double, double, options)
    end

    it 'requires the options to be present' do
      options.keys.each do |opt|
        expect do
          # Hash#except is provided by ActiveSupport, not available in vanilla Ruby
          described_class.new(double, double, double, options.except(opt))
        end.to raise_exception(KeyError)
      end
    end
  end

  describe 'instance methods' do
    let(:args_array) { [1, 2, 3] }
    let(:target_hash) { { id: 123 } }

    before(:each) do
      allow(args_klass).to receive(:new).and_return(args_array)
      allow(target_klass).to receive(:new).and_return(target_hash)
    end

    let(:norad_scan) { described_class.new(double, double, double, options) }

    it 'implements #post_initialize' do
      expect { norad_scan.post_initialize(options) }.not_to raise_error
    end

    describe '#to_h' do
      it 'returns a hash of the targets as an array' do
        expect(norad_scan.to_h[:targets]).to eq([target_hash])
      end

      it 'returns an array of arguments' do
        expect(norad_scan.to_h[:args]).to match_array(args_array)
      end
    end

    describe '#empty?' do
      it 'returns true if the target list is empty' do
        allow(norad_scan).to receive(:target).and_return([])
        expect(norad_scan.empty?).to be(true)
      end

      context 'for a target list of one' do
        it 'returns true if an inner target hash is empty' do
          allow(norad_scan).to receive(:target).and_return([{}])
          expect(norad_scan.empty?).to be(true)
        end

        it 'returns true if an inner target hash has a nil id' do
          allow(norad_scan).to receive(:target).and_return([{ id: nil }])
          expect(norad_scan.empty?).to be(true)
        end

        it 'returns false if an inner target hash has a non-nil id' do
          allow(norad_scan).to receive(:target).and_return([{ id: 'foo' }])
          expect(norad_scan.empty?).to be(false)
        end
      end

      context 'for a target list greater than one' do
        it 'returns true if an inner target hash is empty' do
          allow(norad_scan).to receive(:target).and_return([{ id: 1 }, {}])
          expect(norad_scan.empty?).to be(true)
        end

        it 'returns true if an inner target hash has a nil id' do
          allow(norad_scan).to receive(:target).and_return([{ id: nil }, { id: nil }])
          expect(norad_scan.empty?).to be(true)
        end

        it 'returns false if an inner target hash has a non-nil id' do
          allow(norad_scan).to receive(:target).and_return([{ id: 'foo' }, { id: 'bar' }])
          expect(norad_scan.empty?).to be(false)
        end
      end

      it 'returns true if the args array is empty' do
        allow(norad_scan).to receive(:args).and_return([])
        expect(norad_scan.empty?).to be(true)
      end
    end

    describe 'attribute readers' do
      it 'responds_to :target' do
        expect(norad_scan.respond_to?(:target)).to be(true)
      end

      it 'responds_to :args' do
        expect(norad_scan.respond_to?(:args)).to be(true)
      end

      it 'responds_to :dc' do
        expect(norad_scan.respond_to?(:dc)).to be(true)
      end

      it 'responds_to :container' do
        expect(norad_scan.respond_to?(:container)).to be(true)
      end

      it 'responds_to :secret_id' do
        expect(norad_scan.respond_to?(:secret_id)).to be(true)
      end
    end
  end
end
