# frozen_string_literal: true

# rubocop:disable Metrics/BlockLength
Rails.application.routes.draw do
  if Rails.env.development?
    require 'resque/server'
    mount Resque::Server.new, at: '/resque'
  end
  namespace 'v1' do
    resources :users, only: %i[index show update create] do
      resource 'api_token', only: :update
      post 'authenticate', on: :member
      post 'authenticate', on: :collection
    end
    resources :organizations, except: %i[new edit] do
      resource :scan_summary, only: :show, controller: :organization_scan_summaries
      resources :result_ignore_rules, only: %i[create index destroy]
      resources :docker_commands, only: %i[create index] do
        get 'latest', on: :collection
      end
      resources :machines, only: %i[create index]
      resources :security_container_configs, only: %i[index create]
      resources :docker_relays, only: :index
      resource :scan, only: :create
      resources :memberships, only: %i[create index]
      resources :scan_schedules, only: %i[create index], controller: :organization_scan_schedules
      resource :organization_token, only: :update
      resources :requirements, only: :index, controller: :organization_requirements
      resources :enforcements, only: %i[index create]
      resource :iaas_configuration, only: %i[create show]
      resources :iaas_discoveries, only: %i[create index]
      resources :ssh_key_pairs, only: %i[create index]
      resources :notification_channels, only: :index
      resources :result_export_queues, only: :index
      resources :jira_export_queues, only: :create
      resources :infosec_export_queues, only: :create
      resources :security_containers, only: :index
    end
    resources :machines, only: %i[show update destroy create] do
      resources :ping_connectivity_checks, only: %i[create index show]
      resources :ssh_connectivity_checks, only: %i[create index show]
      resource :scan_summary, only: :show, controller: :machine_scan_summaries
      resources :result_ignore_rules, only: %i[create index destroy]
      resources :docker_commands, only: %i[create index] do
        get 'latest', on: :collection
      end
      resources :assessments, only: :index do
        get 'latest', on: :collection
      end
      resources :security_container_configs, only: %i[index create]
      resources :services, only: %i[index create]
      resources :service_discoveries, only: %i[index create]
      resource :scan, only: :create
      resources :scan_schedules, only: %i[create index], controller: :machine_scan_schedules
      resource :ssh_key_pair_assignment, only: %i[create destroy]
    end
    resources :security_test_repositories, except: %i[new edit] do
      resources :security_containers, only: %i[index create]
      resources :repository_whitelist_entries, only: %i[index create]
      resources :repository_members, only: %i[index create]
    end
    resources :repository_members, only: :destroy
    resources :repository_whitelist_entries, only: :destroy
    resources :result_ignore_rules, only: %i[show update destroy]
    resources :docker_commands, only: %i[show destroy] do
      resource :machine_summary, only: :show, controller: :docker_command_machine_summaries
      resources :assessments, only: :index
    end
    resources :assessments, only: %i[show update] do
      resources :results, only: :create
    end
    resources :docker_relays, only: %i[create update destroy show] do
      resources :errors, only: %i[create destroy], param: :error_klass, controller: :relay_errors
      member do
        post :heartbeat
      end
    end
    resources :security_container_configs, only: %i[show update destroy]
    resources :security_containers, only: %i[index show update destroy]
    resources :services, only: %i[show update destroy] do
      resources :web_application_configs, only: :create
      resources :service_identities, only: :create
    end
    resources :ssh_key_pairs, only: %i[show update destroy]
    resources :notification_channels, only: %i[show update]
    resources :jira_export_queues, only: %i[update show destroy]
    resources :infosec_export_queues, only: %i[update show destroy]
    resources :result_exporters, only: :create
    resources :organization_configurations, only: %i[show update]
    resources :memberships, only: [:destroy]
    resources :machine_scan_schedules, only: %i[update show destroy]
    resources :organization_scan_schedules, only: %i[update show destroy]
    resources :enforcements, only: :destroy
    resources :requirement_groups, only: %i[index show create update destroy] do
      resources :requirements, only: :create
    end
    resources :requirements, only: %i[show update destroy] do
      resources :provisions, only: :create
    end
    resources :organization_errors, only: %i[destroy create]
    resources :provisions, only: :destroy
    resources :iaas_configurations, only: %i[show update destroy]
    resources :iaas_discoveries, only: :show
    resources :service_discoveries, only: %i[show update]
    resources :service_identities, only: %i[update show destroy]
    resources :web_application_configs, only: %i[update show destroy]
    resources :password_resets, only: %i[create update show], param: :provided_token
    resources :email_confirmations, only: :create, param: :email_confirmation_token
    resources :ping_connectivity_checks, only: :update
    resources :ssh_connectivity_checks, only: :update
  end
  mount(ClientTestRouteHelpers::Engine, at: '/client_test_route_helpers') if Rails.env.client_test?
end
# rubocop:enable Metrics/BlockLength
