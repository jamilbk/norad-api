# frozen_string_literal: true

if Rails.env.production?
  Dkim.domain = 'norad.cisco.com'
  Dkim.selector = 'mail'
  Dkim.private_key = open('.dkim/dkim-private.pem').read
  ActionMailer::Base.register_interceptor(Dkim::Interceptor)
end
